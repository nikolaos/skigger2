package org.zoh.skigger.server.controllers.dictionary.model

/**
 * Created by nikolay on 15.10.14.
 */
class Workout {
    String id
    String name
    int seq
    List<Task> tasks
}
