package org.zoh.skigger.server.model.domain

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

/**
 * Created by nikolay on 21.09.14.
 */
@Entity
class Program {
    @Id
    String id
    @Column(length = 40, nullable = false)
    String name
    @OneToMany(mappedBy = 'program', fetch = FetchType.LAZY)
    List<PlanWorkout> workouts
}
