package org.zoh.skigger.server.controllers.dictionary.model

/**
 * Created by nikolay on 15.10.14.
 */
class Attempt {
    String id
    int seq
    int count
    double amount
    int delay
}
