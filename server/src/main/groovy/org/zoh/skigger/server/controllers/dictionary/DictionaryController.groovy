package org.zoh.skigger.server.controllers.dictionary

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by nikolay on 15.10.14.
 */
@RestController
@RequestMapping('/dictionary')
class DictionaryController {
    @Autowired
    DictionaryService dictionaryService

    @RequestMapping('/')
    def index() {
        dictionaryService.allPrograms
    }
}
