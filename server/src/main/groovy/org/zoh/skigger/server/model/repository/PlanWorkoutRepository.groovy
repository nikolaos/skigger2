package org.zoh.skigger.server.model.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.zoh.skigger.server.model.domain.PlanWorkout

/**
 * Created by nikolay on 15.10.14.
 */
@Repository
interface PlanWorkoutRepository extends CrudRepository<PlanWorkout, UUID>{

}