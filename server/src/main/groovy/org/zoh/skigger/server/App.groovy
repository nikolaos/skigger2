package org.zoh.skigger.server

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.orm.jpa.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

/**
 * Created by nikolay on 17.09.14.
 */
@EnableAutoConfiguration
@ComponentScan
@EntityScan
@EnableTransactionManagement
@EnableJpaRepositories
class App {
    public static void main(String[] args) {
        SpringApplication.run(App, args)
    }
}
