package org.zoh.skigger.server.model.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.zoh.skigger.server.model.domain.PlanTask

/**
 * Created by nikolay on 15.10.14.
 */
@Repository
interface PlanTaskRepository extends CrudRepository<PlanTask, UUID> {

}