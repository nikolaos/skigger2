package org.zoh.skigger.server.model.domain

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

/**
 * Created by nikolay on 15.10.14.
 */
@Entity
@Table(name = 'plan_task')
class PlanTask {
    @Id
    String id
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name='workout_id')
    PlanWorkout workout
    @Column(length = 30, nullable = false)
    String name
    @Column
    Integer seq
    @OneToMany(mappedBy = 'task', fetch = FetchType.LAZY)
    List<PlanAttempt> attempts
}
