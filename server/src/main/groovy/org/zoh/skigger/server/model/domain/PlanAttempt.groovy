package org.zoh.skigger.server.model.domain

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

/**
 * Created by nikolay on 15.10.14.
 */
@Entity
@Table(name = "plan_attempt")
class PlanAttempt {
    @Id
    String id
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "task_id")
    PlanTask task
    @Column
    Integer seq
    @Column(name = 'ccount')
    Integer count
    @Column
    Double amount
    @Column
    Integer delay
}
