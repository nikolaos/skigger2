package org.zoh.skigger.server.model.domain

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

/**
 * Created by nikolay on 15.10.14.
 */
@Entity
@Table(name="plan_workout")
class PlanWorkout {
    @Id
    String id
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "program_id")
    Program program
    @Column(length = 30)
    String name
    @Column
    Integer seq
    @OneToMany(mappedBy = 'workout', fetch = FetchType.LAZY)
    List<PlanTask> tasks
}
