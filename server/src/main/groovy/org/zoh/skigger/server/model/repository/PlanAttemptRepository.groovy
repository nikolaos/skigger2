package org.zoh.skigger.server.model.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.zoh.skigger.server.model.domain.PlanAttempt

/**
 * Created by nikolay on 15.10.14.
 */
@Repository
interface PlanAttemptRepository extends CrudRepository<PlanAttempt, UUID> {

}