insert into program (id, name) values (1,'База до отказа');

insert into plan_workout (id, no, name, program_id) values (1, 1, 'День 1. Жим', 1);
insert into plan_workout (id, no, name, program_id) values (2, 2, 'День 2. Присед', 1);
insert into plan_workout (id, no, name, program_id) values (3, 3, 'День 3. Становая', 1);

insert into plan_task (id, no, name, plan_workout_id) values (1, 1, 'Жим лежа', 1);
insert into plan_task (id, no, name, plan_workout_id) values (2, 2, 'Армейский жим', 1);
insert into plan_task (id, no, name, plan_workout_id) values (3, 3, 'Гантели стоя', 1);
insert into plan_task (id, no, name, plan_workout_id) values (4, 4, 'Прес + гиперэкстензия', 1);

insert into plan_task (id, no, name, plan_workout_id) values (5, 1, 'Присед', 2);
insert into plan_task (id, no, name, plan_workout_id) values (6, 2, 'Жим узким хватом', 2);
insert into plan_task (id, no, name, plan_workout_id) values (7, 3, 'Танга на бицепс', 2);
insert into plan_task (id, no, name, plan_workout_id) values (8, 4, 'Гантели стоя', 2);
insert into plan_task (id, no, name, plan_workout_id) values (9, 5, 'Прес', 2);

insert into plan_task (id, no, name, plan_workout_id) values (10, 1, 'Жим с паузами', 3);
insert into plan_task (id, no, name, plan_workout_id) values (11, 2, 'Становая тяга', 3);
insert into plan_task (id, no, name, plan_workout_id) values (12, 3, 'Подтягивания', 3);
insert into plan_task (id, no, name, plan_workout_id) values (13, 4, 'Прес', 3);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (1, 1, 50, 12, 120, 1);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (2, 2, 80, 10, 120, 1);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (3, 3, 80, 8, 120, 1);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (4, 4, 90, 5, 120, 1);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (5, 5, 100, 3, 120, 1);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (6, 6, 105, 2, 120, 1);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (7, 7, 107.5, 2, 120, 1);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (8, 1, 30, 10, 120, 2);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (9, 2, 40, 10, 120, 2);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (10, 3, 40, 10, 120, 2);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (11, 4, 45, 8, 120, 2);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (12, 1, 14, 12, 120, 3);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (13, 2, 16, 10, 120, 3);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (14, 3, 18, 10, 120, 3);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (49, 1, 0, 0, 120, 4);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (50, 2, 0, 0, 120, 4);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (51, 3, 0, 0, 120, 4);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (15, 1, 50, 12, 120, 5);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (16, 2, 80, 10, 120, 5);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (17, 3, 80, 8, 120, 5);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (18, 4, 110, 5, 120, 5);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (19, 5, 110, 3, 120, 5);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (20, 6, 120, 2, 120, 5);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (21, 1, 50, 10, 120, 6);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (22, 2, 60, 10, 120, 6);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (23, 3, 62.5, 10, 120, 6);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (24, 1, 30, 10, 120, 7);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (25, 2, 32.5, 10, 120, 7);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (26, 3, 35, 10, 120, 7);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (27, 4, 35, 8, 120, 7);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (28, 1, 14, 10, 120, 8);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (29, 2, 16, 10, 120, 8);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (30, 3, 16, 10, 120, 8);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (31, 1, 0, 0, 120, 9);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (32, 2, 0, 0, 120, 9);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (33, 3, 0, 0, 120, 9);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (34, 1, 30, 3, 120, 10);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (35, 2, 50, 3, 120, 10);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (36, 3, 90, 3, 120, 10);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (37, 1, 50, 12, 120, 11);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (38, 2, 80, 10, 120, 11);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (39, 3, 80, 8, 120, 11);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (40, 4, 110, 5, 120, 11);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (41, 5, 110, 3, 120, 11);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (42, 6, 120, 3, 120, 11);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (43, 1, 0, 0, 120, 12);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (44, 2, 0, 0, 120, 12);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (45, 3, 0, 0, 120, 12);

insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (46, 1, 0, 0, 120, 13);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (47, 2, 0, 0, 120, 13);
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id) values (48, 3, 0, 0, 120, 13);
