insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id, started, finished) values (100, 8, 110, 1, 120, 1, null, datetime('now', 'localtime', '-3 months'));
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id, started, finished) values (101, 8, 120, 1, 120, 1, datetime('now', 'localtime', '-3 months'),  datetime('now', 'localtime', '+1 days'));
insert into plan_set (id, no, amount, count, rest_time_sec, plan_task_id, started, finished) values (102, 8, 120, 1, 120, 1, datetime('now', 'localtime', '+1 days'), null);
