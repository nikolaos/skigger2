package org.zoh.skigger.model

import org.zoh.skigger.AbstractDbTest

/**
 * Created by nushmodin on 06.08.2014.
 */
class DictionaryTest extends AbstractModelTest {
    def "version is 0"() {
        when:
        def version = skiggerService.getDictionariesVersion()

        then:
        version == 0
    }

    def "new program ver"() {
        setup:
        insertsFrom '/org/zoh/skigger/db/service/new_program_ver.sql'

        when:
        def version = skiggerService.getDictionariesVersion()

        then:
        version == 100
    }
}
