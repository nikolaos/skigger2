package org.zoh.skigger.model

import org.zoh.skigger.AbstractDbTest
import org.zoh.skigger.db.service.SkiggerService

/**
 * Created by nushmodin on 06.06.2014.
 */
class SkipTest extends AbstractModelTest {

    def "skipSet from start"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        model.skipSet()

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.STARTED
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 1
        model.task.no == 1
        model.task.name == 'Жим лежа'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 2
        model.set.no == 2
        model.set.planAmount == 80
        model.set.planCount == 10
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 80
        model.set.nextPlanCount == 8

        when:
        skiggerService.findLastFactSet()

        then:
        thrown(NoSuchElementException)

    }


    def "check skipSet work"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        model.shift()
        model.skipSet()

        then:

        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.WORK
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 1
        model.task.no == 1
        model.task.name == 'Жим лежа'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 2
        model.set.no == 2
        model.set.planAmount == 80
        model.set.planCount == 10
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 80
        model.set.nextPlanCount == 8

    }

    def "check skipSet rest"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        model.shift()
        model.shift()
        model.skipSet()
        def lastFactSet = skiggerService.findLastFactSet()

        then:
        lastFactSet.skipped == true

        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.REST
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 1
        model.task.no == 1
        model.task.name == 'Жим лежа'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 3
        model.set.no == 3
        model.set.planAmount == 80
        model.set.planCount == 8
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 90
        model.set.nextPlanCount == 5

    }


    def "skipTask from start"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        model.skipTask()
        def factTask = skiggerService.findLastFactTask()
        def planTask = skiggerService.queryPlanTask(factTask.getPlanTask().id)

        then:
        planTask.no == 1
        factTask.skipped == true

        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.STARTED
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 2
        model.task.no == 2
        model.task.name == 'Армейский жим'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 8
        model.set.no == 1
        model.set.planAmount == 30
        model.set.planCount == 10
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 40
        model.set.nextPlanCount == 10
    }

    def "skipTask from work"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        model.shift()
        model.skipTask()

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.WORK
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 2
        model.task.no == 2
        model.task.name == 'Армейский жим'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 8
        model.set.no == 1
        model.set.planAmount == 30
        model.set.planCount == 10
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 40
        model.set.nextPlanCount == 10

    }

    def "skip workout"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        model.skipWorkout()
        def hasNext = model.shift()

        then:
        hasNext == false
    }
}
