package org.zoh.skigger.model

import org.zoh.skigger.AbstractDbTest
import org.zoh.skigger.db.service.SkiggerService

/**
 * Created by nushmodin on 06.06.2014.
 */
class CreateInstanceTest extends AbstractModelTest {
    def "create workout model instance by plan workout id"() {
        when:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.STARTED

        model.task.planId == 1
        model.task.no == 1
        model.task.name == 'Жим лежа'

        model.set.planId == 1
        model.set.no == 1
        model.set.planAmount == 50
        model.set.planCount == 12
        model.set.restTimeSec == 120
    }

    def "create workout model instance by plan set id"() {
        when:
        def model = WorkoutModel.createByPlanSet(1, skiggerService)

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.STARTED

        model.task.planId == 1
        model.task.no == 1
        model.task.name == 'Жим лежа'

        model.set.planId == 1
        model.set.no == 1
        model.set.planAmount == 50
        model.set.planCount == 12
        model.set.restTimeSec == 120
    }

    def "instance not created by workout id"() {
        when:
        WorkoutModel.createByWorkout(4, skiggerService)

        then:
        thrown(NoSuchElementException)
    }

    def "instance not created by set id"() {
        when:
        WorkoutModel.createByPlanSet(100, skiggerService)

        then:
        thrown(NoSuchElementException)
    }
}
