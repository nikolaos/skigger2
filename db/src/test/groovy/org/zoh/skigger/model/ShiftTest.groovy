package org.zoh.skigger.model

import org.zoh.skigger.AbstractDbTest
import org.zoh.skigger.db.service.SkiggerService

/**
 * Created by nushmodin on 06.06.2014.
 */
class ShiftTest extends AbstractModelTest {

    def "check shift: start"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        model.shift()

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.WORK
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 1
        model.task.no == 1
        model.task.name == 'Жим лежа'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 1
        model.set.no == 1
        model.set.planAmount == 50
        model.set.planCount == 12
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 80
        model.set.nextPlanCount == 10
    }

    def "check shift: start,rest"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        model.shift()
        model.shift()

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.REST
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 1
        model.task.no == 1
        model.task.name == 'Жим лежа'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 2
        model.set.no == 2
        model.set.planAmount == 80
        model.set.planCount == 10
        model.set.restTimeSec == 120
    }

    def "check shift: start,rest,work"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        model.shift()
        model.shift()
        model.shift()

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.WORK
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 1
        model.task.no == 1
        model.task.name == 'Жим лежа'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 2
        model.set.no == 2
        model.set.planAmount == 80
        model.set.planCount == 10
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 80
        model.set.nextPlanCount == 8
    }

    def "check shift: start,rest,work..next_task"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        15.times { model.shift() }

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.WORK
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 2
        model.task.no == 2
        model.task.name == 'Армейский жим'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 8
        model.set.no == 1
        model.set.planAmount == 30
        model.set.planCount == 10
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 40
        model.set.nextPlanCount == 10
    }

    def "check shift: start,rest,work..next_task..next_task,rest"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        24.times { model.shift() }

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.REST
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 3
        model.task.no == 3
        model.task.name == 'Гантели стоя'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 13
        model.set.no == 2
        model.set.planAmount == 16
        model.set.planCount == 10
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 18
        model.set.nextPlanCount == 10
    }

    def "check shift: start..end"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        32.times { model.shift() }
        def preLastSet = model.shift()
        def hasNext = model.shift()

        then:
        preLastSet == true
        hasNext == false
    }

    def "check createByPlanSet: start,work"() {
        given:
        def model = WorkoutModel.createByPlanSet(1, skiggerService)

        when:
        model.shift()

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.WORK
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 1
        model.task.no == 1
        model.task.name == 'Жим лежа'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 1
        model.set.no == 1
        model.set.planAmount == 50
        model.set.planCount == 12
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 80
        model.set.nextPlanCount == 10
    }

    def "check createByPlanSet: start,work,rest"() {
        given:
        def model = WorkoutModel.createByPlanSet(1, skiggerService)

        when:
        model.shift()
        model.shift()

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.REST
        model.workout.factId != null
        model.workout.created != null

        model.task.planId == 1
        model.task.no == 1
        model.task.name == 'Жим лежа'
        model.task.factId != null
        model.task.created != null

        model.set.planId == 2
        model.set.no == 2
        model.set.planAmount == 80
        model.set.planCount == 10
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 80
        model.set.nextPlanCount == 8
    }

}
