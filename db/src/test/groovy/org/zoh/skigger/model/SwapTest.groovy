package org.zoh.skigger.model

import org.zoh.skigger.AbstractDbTest
import org.zoh.skigger.db.service.SkiggerService
import spock.lang.Specification

/**
 * Created by nushmodin on 03.06.2014.
 */
class SwapTest extends AbstractModelTest {

    def "swap task"() {
        given:
        def model = WorkoutModel.createByWorkout(1, skiggerService)

        when:
        model.swapCurrentTaskWith(10)

        then:
        model.workout.planId == 1
        model.workout.no == 1
        model.workout.state == WorkoutState.STARTED

        model.task.planId == 15
        model.task.no == 1
        model.task.name == 'Жим с паузами'

        model.set.no == 1
        model.set.planAmount == 30
        model.set.planCount == 3
        model.set.restTimeSec == 120
        model.set.nextPlanAmount == 50
        model.set.nextPlanCount == 3
    }
}
