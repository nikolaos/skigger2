package org.zoh.skigger.model

import org.zoh.skigger.AbstractDbTest
import org.zoh.skigger.db.service.SkiggerService

/**
 * Created by nushmodin on 06.06.2014.
 */
class AbstractModelTest extends AbstractDbTest {
    SkiggerService skiggerService

    void setup() {
        skiggerService = new SkiggerService(daoFactory)
    }
}
