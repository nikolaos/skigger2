package org.zoh.skigger.db.dao

import org.zoh.skigger.AbstractDbTest

/**
 * Created by nushmodin on 28.05.2014.
 */
class FactSetDaoTest extends AbstractDbTest {

    def "search last fact set"() {
        setup:
        insertsFrom('/org/zoh/skigger/db/dao/find_last_set.sql')
        when:
        def factSet = daoFactory.factSetDao.findLast()
        then:
        factSet != null
    }
}
