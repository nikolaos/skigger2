package org.zoh.skigger.db
import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.support.ConnectionSource
import org.zoh.skigger.util.Resources
import spock.lang.Specification

/**
 * Created by nushmodin on 28.05.2014.
 */
class DaoFactoryTest extends Specification {
    ConnectionSource connectionSource
    String dbPath = System.properties['java.io.tmpdir']+'/skigger.test.db'

    void setup() {
        new File(dbPath).delete()
        connectionSource = new JdbcConnectionSource("jdbc:sqlite:$dbPath")
    }

    void cleanup() {
        connectionSource.close()
    }

    def "table creations"() {
        when:
        DaoFactory.createTables(connectionSource)
        then:
        noExceptionThrown()
    }

    def "table deletes"() {
        given:
        DaoFactory.createTables(connectionSource)

        when:
        DaoFactory.dropTables(connectionSource)

        then:
        noExceptionThrown()
    }

    def "all dao created"() {
        given:
        DaoFactory.createTables(connectionSource)
        def factory = new DaoFactory(connectionSource)

        when:
        def programDao = factory.programDao
        def planWorkoutDao = factory.planWorkoutDao
        def factWorkoutDao = factory.factWorkoutDao
        def planTaskDao = factory.planTaskDao
        def factTaskDao = factory.factTaskDao
        def planSetDao = factory.planSetDao
        def factSetDao = factory.factSetDao

        then:
        noExceptionThrown()
        programDao != null
        planWorkoutDao != null
        factWorkoutDao != null
        planTaskDao != null
        factTaskDao != null
        planSetDao != null
        factSetDao != null
    }

    def "load test initial data"() {
        given:
        DaoFactory.createTables(connectionSource)
        def connection = connectionSource.getReadWriteConnection()
        Resources.readResourceToStrings("/db/init_data.sql")
                .findAll {!it.trim().isEmpty()}
                .each {
            connection.insert(it, null, null, null)
        }

        when:
        def programs = connection.queryForLong('select count(*) from program')
        def planWorkout = connection.queryForLong('select count(*) from plan_workout')
        def planTask = connection.queryForLong('select count(*) from plan_task')
        def planSet = connection.queryForLong('select count(*) from plan_set')

        then:
        noExceptionThrown()
        programs == 1
        planWorkout == 3
        planTask == 13
        planSet == 51
    }

    def "testResource"() {
        when:
        def res = this.class.getResource("/db/init_data.sql")
        println(res)
        then:
        res != null
    }
}
