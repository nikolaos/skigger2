package org.zoh.skigger.db.service
import org.zoh.skigger.AbstractDbTest

class SkiggerServiceTest extends AbstractDbTest {
    SkiggerService service

    void setup() {
        service = new SkiggerService(daoFactory)
    }

    def "find unfinished set"() {
        setup:
        insertsFrom '/org/zoh/skigger/db/service/unfinished_set.sql'

        when:
        def lastFactSet = service.findLastFactSet()

        then:
        lastFactSet != null
        lastFactSet.id == 1
        lastFactSet.planSet != null
        lastFactSet.factTask != null
    }

    def "find unfinished skipped set"() {
        setup:
        insertsFrom '/org/zoh/skigger/db/service/unfinished_set_skipped.sql'

        when:
        def lastFactSet = service.findLastFactSet()

        then:
        lastFactSet.id == 2
        lastFactSet.planSet.id == 2
        lastFactSet.factTask != null
    }

    def "skipped task not found"() {
        setup:
        insertsFrom '/org/zoh/skigger/db/service/skipped_task.sql'

        when:
        service.findLastFactSet()

        then:
        thrown(NoSuchElementException)
    }

    def "skipped workout not found"() {
        setup:
        insertsFrom '/org/zoh/skigger/db/service/skipped_workout.sql'

        when:
        service.findLastFactSet()

        then:
        thrown(NoSuchElementException)
    }

    def "unfinished set not found"() {
        when:
        service.findLastFactSet()

        then:
        thrown(NoSuchElementException)
    }

    def "simple find next plan set"() {
        when:
        def nextPlanSet = service.findNextPlanSet(2)

        then:
        nextPlanSet != null
        nextPlanSet.id == 3
        nextPlanSet.no == 3
    }

    def "find next plan set with finished"() {
        setup:
        insertsFrom '/org/zoh/skigger/db/service/find_next_plan_set.sql'

        when:
        def nextPlanSet = service.findNextPlanSet(7)

        then:
        nextPlanSet != null
        nextPlanSet.id == 101
        nextPlanSet.no == 8
    }

    def "next plan set not found"() {
        when:
        def nextPlanSet = service.findNextPlanSet(48)

        then:
        thrown(NoSuchElementException)
    }

    def "find next plan set by last plan set"() {
        when:
        def nextPlanSet = service.findNextPlanSet(7)

        then:
        thrown(NoSuchElementException)
    }

    def "find programs"() {
        when:
        def programs = service.getPrograms()

        then:
        ! programs.isEmpty()
    }

    def "find workouts"() {
        when:
        def workouts = service.getPlanWorkouts(1)

        then:
        ! workouts.isEmpty()
    }

    def "find next workout"() {
        setup:
        insertsFrom '/org/zoh/skigger/db/service/next_workout.sql'

        when:
        def planWorkout = service.findNextWorkout()

        then:
        planWorkout != null
        planWorkout.id == 2
        planWorkout.no == 2
    }

    def "simple find next plan task"() {
        given:
        insertsFrom '/org/zoh/skigger/db/service/find_next_task.sql'

        when:
        def planTask = service.findNextTask(1)

        then:
        planTask != null
        planTask.id == 15
        planTask.no == 2
        planTask.name == 'Танга на бицепс'

        when:
        planTask = service.findNextTask(6)

        then:
        planTask != null
        planTask.id == 14
        planTask.no == 3
        planTask.name == 'Армейский жим'

        when:
        planTask = service.findNextTask(2)

        then:
        planTask != null
        planTask.id == 3
        planTask.no == 3
    }

    def "find next plan task by last task"() {
        when:
        service.findNextTask(9)

        then:
        thrown(NoSuchElementException)
    }

    def "swap task"() {
        when:
        def nowTaskA = service.queryPlanTask(2)
        def nowTaskB = service.queryPlanTask(7)

        def nowSetsA = service.queryPlanSetsForTask(nowTaskA.id)
        def nowSetsB = service.queryPlanSetsForTask(nowTaskB.id)

        def result = service.swapPlanTasks(nowTaskA.id, nowTaskB.id)

        def nextTaskA = service.findNextTask(1)
        def nextTaskB = service.findNextTask(6)

        def nextPlanSetsA = service.queryPlanSetsForTask(nextTaskA.id)
        def nextPlanSetsB = service.queryPlanSetsForTask(nextTaskB.id)

        then:
        nowTaskA.no == nextTaskA.no
        nowTaskA.planWorkout.id == nextTaskA.planWorkout.id
        nowTaskA.name != nextTaskA.name
        nowSetsA.size() == nextPlanSetsA.size()
        nowSetsA.collect{ now->
            nextPlanSetsB.findIndexOf{ next-> next.no == now.no && next.count == now.count && Math.abs(next.amount - now.amount) < 0.00001}
        }.count {it == -1} == 0

        nowTaskB.no == nextTaskB.no
        nowTaskB.planWorkout.id == nextTaskB.planWorkout.id
        nowTaskB.name != nextTaskB.name
        nowSetsB.size() == nextPlanSetsB.size()
        nowSetsB.collect{ now->
            nextPlanSetsA.findIndexOf{ next-> next.no == now.no && next.count == now.count && Math.abs(next.amount - now.amount) < 0.00001}
        }.count {it == -1} == 0

        result.newTaskAId == 15
        result.newTaskBId == 14
        result.nextWeekTaskAId == 17
        result.nextWeekTaskBId == 16

        nextTaskA.id == result.newTaskAId
        nextTaskB.id == result.newTaskBId
    }

    def "find first plan task by workout"() {
        when:
        def task = service.findFirstPlanTaskByWorkout(1)

        then:
        task.id == 1


        when:
        task = service.findFirstPlanTaskByWorkout(2)

        then:
        task.id == 5


        when:
        task = service.findFirstPlanTaskByWorkout(3)

        then:
        task.id == 10

        when:
        service.findFirstPlanTaskByWorkout(4)

        then:
        thrown(NoSuchElementException)
    }

    def "find first plan set by task"() {
        when:
        def set = service.findFirstPlanSetByTask(1)

        then:
        set.id == 1

        when:
        set = service.findFirstPlanSetByTask(2)

        then:
        set.id == 8

        when:
        set = service.findFirstPlanSetByTask(6)

        then:
        set.id == 21

        when:
        service.findFirstPlanSetByTask(14)

        then:
        thrown(NoSuchElementException)
    }
}
