package org.zoh.skigger
import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.support.DatabaseConnection
import org.zoh.skigger.db.DaoFactory
import org.zoh.skigger.util.Resources
import spock.lang.Specification

/**
 * Created by nushmodin on 28.05.2014.
 */
class AbstractDbTest extends Specification {

    ConnectionSource connectionSource
    DaoFactory daoFactory

    void setup() {
//        String dbPath = System.properties['java.io.tmpdir']+'/skigger.test.db'
//        new File(dbPath).delete()
//        connectionSource = new JdbcConnectionSource("jdbc:sqlite:$dbPath")
        connectionSource = new JdbcConnectionSource("jdbc:sqlite::memory:")
        DaoFactory.createTables(connectionSource)
        insertsFrom('/db/init_data.sql')
        daoFactory = new DaoFactory(connectionSource)
    }

    void cleanup() {
        connectionSource.close()
    }

    def insertsFrom(String resource) {
        DatabaseConnection connection = connectionSource.getReadWriteConnection()
        Resources.readResourceToStrings(resource)
                .findAll { !it.trim().isEmpty() }
                .each {
            connection.insert(it, null, null, null)
        }
    }
}
