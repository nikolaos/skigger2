package org.zoh.skigger.model;

import org.zoh.skigger.db.entity.*;
import org.zoh.skigger.db.service.SkiggerService;
import org.zoh.skigger.db.service.SwapPlanTaskResult;
import org.zoh.skigger.util.Dates;

import java.util.Calendar;
import java.util.Date;
import java.util.NoSuchElementException;

/**
 * Created by nushmodin on 03.06.2014.
 */
public class WorkoutModel {
    private final SkiggerService skiggerService;
    private Workout workout;
    private Task task;
    private Set set;
    private boolean hasNext = true;

    private WorkoutModel(SkiggerService skiggerService) {
        this.skiggerService = skiggerService;
    }

    public static WorkoutModel createByWorkout(long workoutId, SkiggerService skiggerService) {
        WorkoutModel model = new WorkoutModel(skiggerService);

        PlanWorkout planWorkout = skiggerService.queryPlanWorkout(workoutId);
        model.workout = new Workout();
        model.workout.setPlanId(planWorkout.getId());
        model.workout.setNo(planWorkout.getNo());
        model.workout.setState(WorkoutState.STARTED);

        PlanTask planTask = skiggerService.findFirstPlanTaskByWorkout(workoutId);
        model.task = new Task();
        model.task.setPlanId(planTask.getId());
        model.task.setNo(planTask.getNo());
        model.task.setName(planTask.getName());

        PlanSet planSet = skiggerService.findFirstPlanSetByTask(planTask.getId());
        model.set = new Set();
        model.set.setPlanId(planSet.getId());
        model.set.setNo(planSet.getNo());
        model.set.setPlanAmount(planSet.getAmount());
        model.set.setPlanCount(planSet.getCount());
        model.set.setFactAmount(planSet.getAmount());
        model.set.setFactCount(planSet.getCount());
        model.set.setRestTimeSec(planSet.getRestTimeSec());

        try {
            PlanSet nextPlanSet = skiggerService.findNextPlanSet(planSet.getId());
            model.set.setNextPlanAmount(nextPlanSet.getAmount());
            model.set.setNextPlanCount(nextPlanSet.getCount());
        } catch (NoSuchElementException e) {
            model.set.setNextPlanAmount(null);
            model.set.setNextPlanCount(null);
        }

        return model;
    }

    public static WorkoutModel createByPlanSet(long planSetId, SkiggerService skiggerService) {
        WorkoutModel model = new WorkoutModel(skiggerService);

        PlanSet planSet = skiggerService.queryPlanSet(planSetId);
        model.set = new Set();
        model.set.setPlanId(planSet.getId());
        model.set.setNo(planSet.getNo());
        model.set.setPlanAmount(planSet.getAmount());
        model.set.setPlanCount(planSet.getCount());
        model.set.setFactAmount(planSet.getAmount());
        model.set.setFactCount(planSet.getCount());
        model.set.setRestTimeSec(planSet.getRestTimeSec());

        try {
            PlanSet nextPlanSet = skiggerService.findNextPlanSet(planSet.getId());
            model.set.setNextPlanAmount(nextPlanSet.getAmount());
            model.set.setNextPlanCount(nextPlanSet.getCount());
        } catch (NoSuchElementException e) {
            model.set.setNextPlanAmount(null);
            model.set.setNextPlanCount(null);
        }

        PlanTask planTask = skiggerService.queryPlanTask(planSet.getPlanTask().getId());
        model.task = new Task();
        model.task.setPlanId(planTask.getId());
        model.task.setNo(planTask.getNo());
        model.task.setName(planTask.getName());

        PlanWorkout planWorkout = skiggerService.queryPlanWorkout(planTask.getPlanWorkout().getId());
        model.workout = new Workout();
        model.workout.setPlanId(planWorkout.getId());
        model.workout.setNo(planWorkout.getNo());
        model.workout.setState(WorkoutState.STARTED);

        return model;
    }

    public boolean shift() {
        if (!hasNext) {
            return false;
        }
        Date now = Dates.truncate(Dates.now(), Calendar.MILLISECOND);
        switch (workout.getState()) {
            case REST:
                set.setPlanRestEndTime(null);
                set.setCreated(now);
                workout.setState(WorkoutState.WORK);
                break;
            case STARTED:
                if (task.getFactId() == null) {
                    startWorkout(now);
                }

                workout.setState(WorkoutState.WORK);
                break;
            case WORK:
                shiftSet(now, null);
                workout.setState(WorkoutState.REST);
                break;
        }
        return hasNext;
    }

    private void shiftSet(Date now, Boolean skipped) {
        FactSet factSet = skiggerService.createFactSet(set.getPlanId(), task.getFactId(), set.getFactAmount(), set.getFactCount(), set.getCreated(), skipped);
        set.setFactId(factSet.getId());

        PlanSet nextPlanSet;
        PlanTask nextPlanTask;
        try {
            try {
                nextPlanSet = skiggerService.findNextPlanSet(set.getPlanId());
            } catch (NoSuchElementException e) {
                nextPlanSet = skiggerService.findFirstSetOfNextTask(set.getPlanId());
            }
            if (!set.getPlanId().equals(nextPlanSet.getPlanTask().getId())) {
                nextPlanTask = skiggerService.queryPlanTask(nextPlanSet.getPlanTask().getId());
            } else {
                nextPlanTask = null;
            }
        } catch (NoSuchElementException e) {
            nextPlanTask = null;
            nextPlanSet = null;
        }

        if (nextPlanTask != null) {
            task.setPlanId(nextPlanTask.getId());
            task.setNo(nextPlanTask.getNo());
            task.setName(nextPlanTask.getName());
            task.setCreated(now);
        }

        if (nextPlanSet != null) {
            set.setPlanId(nextPlanSet.getId());
            set.setNo(nextPlanSet.getNo());
            set.setPlanAmount(nextPlanSet.getAmount());
            set.setPlanCount(nextPlanSet.getCount());
            set.setFactAmount(nextPlanSet.getAmount());
            set.setFactCount(nextPlanSet.getCount());
            set.setRestTimeSec(nextPlanSet.getRestTimeSec());
            set.setPlanRestEndTime(new Date(now.getTime() + nextPlanSet.getRestTimeSec() * 1000));
            set.setCreated(now);

            try {
                PlanSet afterNextPlanSet = skiggerService.findNextPlanSet(nextPlanSet.getId());
                set.setNextPlanAmount(afterNextPlanSet.getAmount());
                set.setNextPlanCount(afterNextPlanSet.getCount());
            } catch (NoSuchElementException e) {
                set.setNextPlanAmount(null);
                set.setNextPlanCount(null);
            }
        }

        hasNext = nextPlanSet != null;
    }

    public void skipSet() {
        Date now = Dates.truncate(Dates.now(), Calendar.MILLISECOND);
        switch (workout.getState()) {
            case STARTED:
                startWorkout(now);

                shiftSet(now, true);
                workout.setState(WorkoutState.STARTED);
                break;
            case WORK:
                shiftSet(now, true);
                workout.setState(WorkoutState.WORK);
                break;
            case REST:
                shiftSet(now, true);
                workout.setState(WorkoutState.REST);
                break;
        }
    }

    private void startWorkout(Date now) {
        FactWorkout factWorkout = skiggerService.createFactWorkout(workout.getPlanId(), now);
        workout.setFactId(factWorkout.getId());
        workout.setCreated(now);

        FactTask factTask = skiggerService.createFactTask(task.getPlanId(), workout.getFactId(), now, null);
        task.setFactId(factTask.getId());
        task.setCreated(now);
    }

    public void skipTask() {
        Date now = Dates.truncate(Dates.now(), Calendar.MILLISECOND);
        if (task.getFactId() == null) {
            startWorkout(now);
        }
        skiggerService.markTaskSkipped(task.getFactId(), true);

        PlanTask planTask = skiggerService.findNextTask(task.getPlanId());
        task.setPlanId(planTask.getId());
        task.setNo(planTask.getNo());
        task.setName(planTask.getName());
        task.setCreated(now);

        PlanSet planSet = skiggerService.findFirstPlanSetByTask(planTask.getId());
        set = new Set();
        set.setPlanId(planSet.getId());
        set.setNo(planSet.getNo());
        set.setPlanAmount(planSet.getAmount());
        set.setPlanCount(planSet.getCount());
        set.setFactAmount(planSet.getAmount());
        set.setFactCount(planSet.getCount());
        set.setRestTimeSec(planSet.getRestTimeSec());
        set.setCreated(now);

        try {
            PlanSet afterNextPlanSet = skiggerService.findNextPlanSet(planSet.getId());
            set.setNextPlanAmount(afterNextPlanSet.getAmount());
            set.setNextPlanCount(afterNextPlanSet.getCount());
        } catch (NoSuchElementException e) {
            set.setNextPlanAmount(null);
            set.setNextPlanCount(null);
        }
    }

    public void skipWorkout() {
        Date now = Dates.truncate(Dates.now(), Calendar.MILLISECOND);
        if (task.getFactId() == null) {
            FactWorkout factWorkout = skiggerService.createFactWorkout(workout.getPlanId(), now);
            workout.setFactId(factWorkout.getId());
            workout.setCreated(now);
        }
        skiggerService.markWorkoutSkipped(workout.getFactId(), true);
        hasNext = false;
    }

    public void swapCurrentTaskWith(long taskId) {
        Date now = Dates.truncate(Dates.now(), Calendar.MILLISECOND);

        SwapPlanTaskResult swapPlanTaskResult = skiggerService.swapPlanTasks(task.getPlanId(), taskId);

        PlanTask planTask = skiggerService.queryPlanTask(swapPlanTaskResult.getNewTaskAId());
        task.setPlanId(planTask.getId());
        task.setNo(planTask.getNo());
        task.setName(planTask.getName());
        task.setCreated(now);

        PlanSet planSet = skiggerService.findFirstPlanSetByTask(planTask.getId());
        set = new Set();
        set.setPlanId(planSet.getId());
        set.setNo(planSet.getNo());
        set.setPlanAmount(planSet.getAmount());
        set.setPlanCount(planSet.getCount());
        set.setFactAmount(planSet.getAmount());
        set.setFactCount(planSet.getCount());
        set.setRestTimeSec(planSet.getRestTimeSec());
        set.setCreated(now);

        try {
            PlanSet afterNextPlanSet = skiggerService.findNextPlanSet(planSet.getId());
            set.setNextPlanAmount(afterNextPlanSet.getAmount());
            set.setNextPlanCount(afterNextPlanSet.getCount());
        } catch (NoSuchElementException e) {
            set.setNextPlanAmount(null);
            set.setNextPlanCount(null);
        }
    }

    public Workout getWorkout() {
        return workout;
    }

    public Task getTask() {
        return task;
    }

    public Set getSet() {
        return set;
    }
}
