package org.zoh.skigger.model;

public enum WorkoutState {
    STARTED, WORK, REST
}
