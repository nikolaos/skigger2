package org.zoh.skigger.model;

import java.util.Date;

/**
 * Created by nushmodin on 03.06.2014.
 */
public class Set {
    private Long planId;
    private Long factId;
    private int no;
    private float planAmount;
    private Float nextPlanAmount;
    private float factAmount;
    private int planCount;
    private Integer nextPlanCount;
    private int factCount;
    private Date created;
    private Date planRestEndTime;
    private int restTimeSec;

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getFactId() {
        return factId;
    }

    public void setFactId(Long factId) {
        this.factId = factId;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public float getPlanAmount() {
        return planAmount;
    }

    public void setPlanAmount(float planAmount) {
        this.planAmount = planAmount;
    }

    public float getFactAmount() {
        return factAmount;
    }

    public void setFactAmount(float factAmount) {
        this.factAmount = factAmount;
    }

    public int getPlanCount() {
        return planCount;
    }

    public void setPlanCount(int planCount) {
        this.planCount = planCount;
    }

    public int getFactCount() {
        return factCount;
    }

    public void setFactCount(int factCount) {
        this.factCount = factCount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getPlanRestEndTime() {
        return planRestEndTime;
    }

    public void setPlanRestEndTime(Date planRestEndTime) {
        this.planRestEndTime = planRestEndTime;
    }

    public int getRestTimeSec() {
        return restTimeSec;
    }

    public void setRestTimeSec(int restTimeSec) {
        this.restTimeSec = restTimeSec;
    }

    public Float getNextPlanAmount() {
        return nextPlanAmount;
    }

    public void setNextPlanAmount(Float nextPlanAmount) {
        this.nextPlanAmount = nextPlanAmount;
    }

    public Integer getNextPlanCount() {
        return nextPlanCount;
    }

    public void setNextPlanCount(Integer nextPlanCount) {
        this.nextPlanCount = nextPlanCount;
    }
}
