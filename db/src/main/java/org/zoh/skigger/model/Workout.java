package org.zoh.skigger.model;

import java.util.Date;

/**
 * Created by nushmodin on 03.06.2014.
 */
public class Workout {
    private Long planId;
    private Long factId;
    private int no;
    private WorkoutState state;
    private Date created;

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getFactId() {
        return factId;
    }

    public void setFactId(Long factId) {
        this.factId = factId;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public WorkoutState getState() {
        return state;
    }

    public void setState(WorkoutState state) {
        this.state = state;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
