package org.zoh.skigger.db;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.zoh.skigger.db.dao.*;
import org.zoh.skigger.db.entity.*;

import java.sql.SQLException;

/**
 * Created by nikolay on 25.05.14.
 */
public class DaoFactory {
    private final ProgramDao     programDao;
    private final PlanWorkoutDao planWorkoutDao;
    private final FactWorkoutDao factWorkoutDao;
    private final PlanTaskDao    planTaskDao;
    private final FactTaskDao    factTaskDao;
    private final PlanSetDao     planSetDao;
    private final FactSetDao     factSetDao;

    public DaoFactory(ConnectionSource source) {
        try {
            final BaseDaoImpl<Program, Long> programDao = DaoManager.createDao(source, Program.class);
            //programDao.initialize();
            this.programDao = new ProgramDao(programDao);

            final BaseDaoImpl<PlanWorkout, Long> planWorkoutsDao = DaoManager.createDao(source, PlanWorkout.class);
            //planWorkoutsDao.initialize();
            planWorkoutDao = new PlanWorkoutDao(planWorkoutsDao);

            final BaseDaoImpl<FactWorkout, Long> factWorkoutsDao = DaoManager.createDao(source, FactWorkout.class);
            //factWorkoutsDao.initialize();
            factWorkoutDao = new FactWorkoutDao(factWorkoutsDao);

            final BaseDaoImpl<PlanTask, Long> planTasksDao = DaoManager.createDao(source, PlanTask.class);
            //planTasksDao.initialize();
            planTaskDao = new PlanTaskDao(planTasksDao);


            final BaseDaoImpl<FactTask, Long> factTasksDao = DaoManager.createDao(source, FactTask.class);
            //factTasksDao.initialize();
            factTaskDao = new FactTaskDao(factTasksDao);

            final BaseDaoImpl<PlanSet, Long> planSetsDao = DaoManager.createDao(source, PlanSet.class);
            //planSetsDao.initialize();
            planSetDao = new PlanSetDao(planSetsDao);


            final BaseDaoImpl<FactSet, Long> factSetsDao = DaoManager.createDao(source, FactSet.class);
            //factSetsDao.initialize();
            factSetDao = new FactSetDao(factSetsDao);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void createTables(ConnectionSource connectionSource) throws SQLException {
        TableUtils.createTable(connectionSource, Program.class);
        TableUtils.createTable(connectionSource, PlanWorkout.class);
        TableUtils.createTable(connectionSource, PlanTask.class);
        TableUtils.createTable(connectionSource, PlanSet.class);
        TableUtils.createTable(connectionSource, FactWorkout.class);
        TableUtils.createTable(connectionSource, FactTask.class);
        TableUtils.createTable(connectionSource, FactSet.class);
    }

    public static void dropTables(ConnectionSource connectionSource) throws SQLException {
        TableUtils.dropTable(connectionSource, FactSet.class, true);
        TableUtils.dropTable(connectionSource, FactTask.class, true);
        TableUtils.dropTable(connectionSource, FactWorkout.class, true);
        TableUtils.dropTable(connectionSource, PlanSet.class, true);
        TableUtils.dropTable(connectionSource, PlanTask.class, true);
        TableUtils.dropTable(connectionSource, PlanWorkout.class, true);
        TableUtils.dropTable(connectionSource, Program.class, true);
    }



    public ProgramDao getProgramDao() {
        return programDao;
    }

    public PlanWorkoutDao getPlanWorkoutDao() {
        return planWorkoutDao;
    }

    public FactWorkoutDao getFactWorkoutDao() {
        return factWorkoutDao;
    }

    public PlanTaskDao getPlanTaskDao() {
        return planTaskDao;
    }

    public FactTaskDao getFactTaskDao() {
        return factTaskDao;
    }

    public PlanSetDao getPlanSetDao() {
        return planSetDao;
    }

    public FactSetDao getFactSetDao() {
        return factSetDao;
    }
}
