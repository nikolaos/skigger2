package org.zoh.skigger.db.entity;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by nushmodin on 23.05.2014.
 */
@DatabaseTable(tableName = "fact_set")
public class FactSet {
    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField(foreign = true, columnName = "fact_task_id", canBeNull = false)
    private FactTask factTask;
    @DatabaseField(foreign = true, columnName = "plan_set_id", canBeNull = false)
    private PlanSet planSet;
    @DatabaseField(dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    private Date created;
    @DatabaseField(canBeNull = false)
    private float amount;
    @DatabaseField(canBeNull = false)
    private int count;
    @DatabaseField
    private Boolean skipped;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FactTask getFactTask() {
        return factTask;
    }

    public void setFactTask(FactTask factTask) {
        this.factTask = factTask;
    }

    public PlanSet getPlanSet() {
        return planSet;
    }

    public void setPlanSet(PlanSet planSet) {
        this.planSet = planSet;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Boolean getSkipped() {
        return skipped;
    }

    public void setSkipped(Boolean skipped) {
        this.skipped = skipped;
    }
}
