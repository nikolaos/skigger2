package org.zoh.skigger.db.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.Where;
import org.zoh.skigger.db.entity.PlanTask;
import org.zoh.skigger.util.StringResourcesCache;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by nushmodin on 23.05.2014.
 */
public class PlanTaskDao extends AbstractDao<PlanTask,Long> {
    public PlanTaskDao(Dao<PlanTask, Long> dao) throws SQLException {
        super(dao);
    }

    public PlanTask findNext(long prevTaskId) {
        try {
            long nextTaskId = queryRawLong(StringResourcesCache.get("/db/v1/query/find_next_task.sql"), String.valueOf(prevTaskId));
            return dao.queryForId(nextTaskId);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<PlanTask> findTasksForWorkout(long workout_id) {
        try {
            return dao.queryForEq("plan_workout_id", workout_id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public PlanTask findFirstPlanTaskByWorkout(long workoutId) {
        try {
            long taskId = queryRawLong(StringResourcesCache.get("/db/v1/query/find_first_task.sql"), String.valueOf(workoutId));
            return dao.queryForId(taskId);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public long findMaxVersion() {
        try {
            return queryRawLong("select max(version) from plan_task");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
