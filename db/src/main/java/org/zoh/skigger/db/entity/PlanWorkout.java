package org.zoh.skigger.db.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by nushmodin on 23.05.2014.
 */
@DatabaseTable(tableName = "plan_workout")
public class PlanWorkout {
    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField(canBeNull = false, defaultValue = "0")
    private long version;
    @DatabaseField
    private int no;
    @DatabaseField(width = 30)
    private String name;
    @DatabaseField(foreign = true, columnName = "program_id", canBeNull = false)
    private Program program;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
}
