package org.zoh.skigger.db.entity;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by nushmodin on 23.05.2014.
 */
@DatabaseTable(tableName = "plan_task")
public class PlanTask {
    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField(canBeNull = false, defaultValue = "0")
    private long version;
    @DatabaseField(width = 30, canBeNull = false)
    private String name;
    @DatabaseField(canBeNull = false)
    private int no;
    @DatabaseField(dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    private Date started;
    @DatabaseField(dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    private Date finished;
    @DatabaseField(foreign = true, columnName = "plan_workout_id", canBeNull = false)
    private PlanWorkout planWorkout;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public Date getStarted() {
        return started;
    }

    public void setStarted(Date started) {
        this.started = started;
    }

    public PlanWorkout getPlanWorkout() {
        return planWorkout;
    }

    public void setPlanWorkout(PlanWorkout planWorkout) {
        this.planWorkout = planWorkout;
    }

    public Date getFinished() {
        return finished;
    }

    public void setFinished(Date finished) {
        this.finished = finished;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
}
