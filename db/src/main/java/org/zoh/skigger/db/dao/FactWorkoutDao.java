package org.zoh.skigger.db.dao;

import com.j256.ormlite.dao.Dao;
import org.zoh.skigger.db.entity.FactWorkout;

import java.sql.SQLException;

/**
 * Created by nushmodin on 23.05.2014.
 */
public class FactWorkoutDao extends AbstractDao<FactWorkout, Long> {
    public FactWorkoutDao(Dao<FactWorkout, Long> dao) throws SQLException {
        super(dao);
    }

    public int markWorkoutSkipped(long factWorkoutId, boolean skipped) {
        try {
            return dao.updateRaw("update fact_workout set skipped = ? where id = ?", skipped ? "1" : "0", String.valueOf(factWorkoutId));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
