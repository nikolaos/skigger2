package org.zoh.skigger.db.dao;

import com.j256.ormlite.dao.Dao;
import org.zoh.skigger.db.entity.FactSet;
import org.zoh.skigger.util.StringResourcesCache;

import java.sql.SQLException;

/**
 * Created by nushmodin on 23.05.2014.
 */
public class FactSetDao extends AbstractDao<FactSet, Long> {
    public FactSetDao(Dao<FactSet, Long> dao) throws SQLException {
        super(dao);
    }

    public FactSet findLast() {
        try {
            long id = queryRawLong(StringResourcesCache.get("/db/v1/query/find_last_set.sql"));
            return dao.queryForId(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
