package org.zoh.skigger.db.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RawRowMapper;

import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by nushmodin on 23.05.2014.
 */
public class AbstractDao<T, ID> {

    protected Dao<T, ID> dao;

    public AbstractDao(Dao<T, ID> dao) throws SQLException {
        this.dao = dao;
    }

    public List<T> queryForAll() {
        try {
            return dao.queryForAll();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public T queryForId(ID id) {
        try {
            T result = dao.queryForId(id);
            if (result == null) {
                throw new NoSuchElementException("Can't find entity for id = " + id);
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int create(T data) {
        try {
            return dao.create(data);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int update(T data) {
        try {
            return dao.update(data);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int refresh(T data) {
        try {
            return dao.refresh(data);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int delete(T data) {
        try {
            return dao.delete(data);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int deleteById(ID id) {
        try {
            return dao.deleteById(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected long queryRawLong(String query, String ...args) throws SQLException{
        List<Long> results = dao.queryRaw(query, new RawRowMapper<Long>() {
            @Override
            public Long mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                try {
                    return Long.parseLong(resultColumns[0]);
                } catch (NumberFormatException e) {
                    throw new SQLException("Can't parse first column", e);
                }
            }
        }, args).getResults();

        if (results.isEmpty()) {
            throw new NoSuchElementException("No data found for query " + query);
        }

        return results.get(0);
    }
}

