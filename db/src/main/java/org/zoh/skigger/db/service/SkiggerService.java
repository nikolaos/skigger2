package org.zoh.skigger.db.service;

import org.zoh.skigger.db.DaoFactory;
import org.zoh.skigger.db.entity.*;
import org.zoh.skigger.util.Dates;
import org.zoh.skigger.util.Nums;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

/**
 *
 * @author nushmodin
 */
public class SkiggerService {
    private final DaoFactory daoFactory;

    public SkiggerService(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    
    public FactSet findLastFactSet() {
        return daoFactory.getFactSetDao().findLast();
    }

    public PlanSet findNextPlanSet(long planSetId) {
        return daoFactory.getPlanSetDao().findNext(planSetId);
    }

    public PlanSet findFirstSetOfNextTask(long planSetId) {
        return daoFactory.getPlanSetDao().findFirstSetOfNextTask(planSetId);
    }

    public List<PlanWorkout> getPlanWorkouts(long programId) {
        return daoFactory.getPlanWorkoutDao().findByProgramId(programId);
    }

    public PlanWorkout findNextWorkout() {
        return daoFactory.getPlanWorkoutDao().findNext();
    }

    public List<Program> getPrograms() {
        return daoFactory.getProgramDao().queryForAll();
    }

    public PlanTask findNextTask(long prevTaskId) {
        return daoFactory.getPlanTaskDao().findNext(prevTaskId);
    }

    public FactSet createFactSet(long planSetId, long factTaskId, float amount, int count, Date created, Boolean skipped) {
        FactSet factSet = new FactSet();
        factSet.setAmount(amount);
        factSet.setCount(count);
        factSet.setCreated(created);

        PlanSet planSet = new PlanSet();
        planSet.setId(planSetId);
        factSet.setPlanSet(planSet);

        FactTask factTask = new FactTask();
        factTask.setId(factTaskId);
        factSet.setFactTask(factTask);

        factSet.setSkipped(skipped);

        daoFactory.getFactSetDao().create(factSet);

        return factSet;
    }

    public FactSet queryFactSet(Long aLong) {
        return daoFactory.getFactSetDao().queryForId(aLong);
    }

    public FactWorkout queryFactWorkout(Long aLong) {
        return daoFactory.getFactWorkoutDao().queryForId(aLong);
    }

    public FactWorkout createFactWorkout(long planWorkoutId, Date created) {
        FactWorkout factWorkout = new FactWorkout();
        factWorkout.setCreated(created);

        PlanWorkout planWorkout = new PlanWorkout();
        planWorkout.setId(planWorkoutId);
        factWorkout.setPlanWorkout(planWorkout);

        daoFactory.getFactWorkoutDao().create(factWorkout);

        return factWorkout;
    }

    public FactTask queryFactTask(Long aLong) {
        return daoFactory.getFactTaskDao().queryForId(aLong);
    }

    public FactTask createFactTask(long planTaskId, long factWorkoutId, Date created, Boolean skipped) {
        FactTask data = new FactTask();
        data.setCreated(created);

        FactWorkout factWorkout = new FactWorkout();
        factWorkout.setId(factWorkoutId);
        data.setFactWorkout(factWorkout);

        PlanTask planTask = new PlanTask();
        planTask.setId(planTaskId);
        data.setPlanTask(planTask);

        data.setSkipped(skipped);

        daoFactory.getFactTaskDao().create(data);

        return data;
    }

    public PlanWorkout queryPlanWorkout(long id) {
        return daoFactory.getPlanWorkoutDao().queryForId(id);
    }

    public SwapPlanTaskResult swapPlanTasks(long taskIdA, long taskIdB) {
        PlanTask planTaskA = daoFactory.getPlanTaskDao().queryForId(taskIdA);
        PlanTask planTaskB = daoFactory.getPlanTaskDao().queryForId(taskIdB);

        Date now = new Date();

        swapPlanTasks(planTaskA, planTaskB, now);
        long newTaskIdB = planTaskA.getId();
        long newTaskIdA = planTaskB.getId();


        Date nextWeek = Dates.addWeek(now);

        swapPlanTasks(planTaskA, planTaskB, nextWeek);
        long nextWeekTaskBId = planTaskA.getId();
        long nextWeekTaskAId = planTaskB.getId();

        return new SwapPlanTaskResult(newTaskIdA, newTaskIdB, nextWeekTaskAId, nextWeekTaskBId);
    }

    private void swapPlanTasks(PlanTask planTaskA, PlanTask planTaskB, Date now) {
        long oldIdA = planTaskA.getId();
        long oldIdB = planTaskB.getId();

        planTaskA.setId(null);
        planTaskB.setId(null);
        planTaskA.setStarted(now);
        planTaskB.setStarted(now);

        int taskNoA = planTaskA.getNo();
        int taskNoB = planTaskB.getNo();
        planTaskA.setNo(taskNoB);
        planTaskB.setNo(taskNoA);

        PlanWorkout planWorkoutA = planTaskA.getPlanWorkout();
        PlanWorkout planWorkoutB = planTaskB.getPlanWorkout();
        planTaskA.setPlanWorkout(planWorkoutB);
        planTaskB.setPlanWorkout(planWorkoutA);

        daoFactory.getPlanTaskDao().create(planTaskA);
        List<PlanSet> planSetsA = daoFactory.getPlanSetDao().gePlanSetsForTask(oldIdA);
        daoFactory.getPlanTaskDao().create(planTaskB);
        List<PlanSet> planSetsB = daoFactory.getPlanSetDao().gePlanSetsForTask(oldIdB);

        for (PlanSet item : planSetsA) {
            item.setPlanTask(planTaskA);
            daoFactory.getPlanSetDao().create(item);
        }

        for (PlanSet item : planSetsB) {
            item.setPlanTask(planTaskB);
            daoFactory.getPlanSetDao().create(item);
        }
    }

    public PlanTask findFirstPlanTaskByWorkout(long workoutId) {
        return daoFactory.getPlanTaskDao().findFirstPlanTaskByWorkout(workoutId);
    }

    public PlanSet findFirstPlanSetByTask(long taskId) {
        return daoFactory.getPlanSetDao().findFirstPlanSetByTask(taskId);
    }

    public PlanSet queryPlanSet(long planSetId) {
        return daoFactory.getPlanSetDao().queryForId(planSetId);
    }

    List<PlanSet> queryPlanSetsForTask(long planTaskId) {
        return daoFactory.getPlanSetDao().gePlanSetsForTask(planTaskId);
    }

    public void markTaskSkipped(long factTaskId, boolean skipped) {
        daoFactory.getFactTaskDao().markTaskSkipped(factTaskId, skipped);
    }

    public void markWorkoutSkipped(long factWorkoutId, boolean skipped) {
        daoFactory.getFactWorkoutDao().markWorkoutSkipped(factWorkoutId, skipped);
    }

    public long getDictionariesVersion() {
        long setVer = daoFactory.getPlanSetDao().findMaxVersion();
        long taskVer = daoFactory.getPlanTaskDao().findMaxVersion();
        long workoutVer = daoFactory.getPlanWorkoutDao().findMaxVersion();
        long programVer = daoFactory.getProgramDao().findMaxVersion();
        return Nums.max(setVer, taskVer, workoutVer, programVer);
    }

    public void update(List<Program> programs, List<PlanWorkout> workouts, List<PlanTask> tasks, List<PlanSet> sets) {
        for (Program program : programs) {
            daoFactory.getProgramDao().create(program);
        }
        for (PlanWorkout planWorkout : workouts) {
            daoFactory.getPlanWorkoutDao().create(planWorkout);
        }
        for (PlanTask planTask : tasks) {
            daoFactory.getPlanTaskDao().create(planTask);
        }
        for (PlanSet planSet : sets) {
            daoFactory.getPlanSetDao().create(planSet);
        }
    }

    //XXX: For tests
    public PlanTask queryPlanTask(Long aLong) {
        return daoFactory.getPlanTaskDao().queryForId(aLong);
    }

    List<PlanTask> queryPlanTasksForWorkout(long workoutId) {
        return daoFactory.getPlanTaskDao().findTasksForWorkout(workoutId);
    }

    FactTask findLastFactTask() {
        return daoFactory.getFactTaskDao().findLastTask();
    }
}
