package org.zoh.skigger.db.entity;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by nushmodin on 23.05.2014.
 */
@DatabaseTable(tableName = "plan_set")
public class PlanSet {
    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField(canBeNull = false, defaultValue = "0")
    private long version;
    @DatabaseField(canBeNull = false)
    private float amount;
    @DatabaseField(canBeNull = false)
    private int count;
    @DatabaseField(dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    private Date finished;
    @DatabaseField(dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    private Date started;
    @DatabaseField(canBeNull = false)
    private int no;
    @DatabaseField(canBeNull = false, columnName = "rest_time_sec")
    private int restTimeSec;
    @DatabaseField(foreign = true, columnName = "plan_task_id", canBeNull = false)
    private PlanTask planTask;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Date getStarted() {
        return started;
    }

    public void setStarted(Date started) {
        this.started = started;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getRestTimeSec() {
        return restTimeSec;
    }

    public void setRestTimeSec(int restTimeSec) {
        this.restTimeSec = restTimeSec;
    }

    public PlanTask getPlanTask() {
        return planTask;
    }

    public void setPlanTask(PlanTask planTask) {
        this.planTask = planTask;
    }

    public void setFinished(Date finished) {
        this.finished = finished;
    }

    public Date getFinished() {
        return finished;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
}
