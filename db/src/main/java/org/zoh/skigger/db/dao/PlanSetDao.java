package org.zoh.skigger.db.dao;

import com.j256.ormlite.dao.Dao;
import org.zoh.skigger.db.entity.PlanSet;
import org.zoh.skigger.util.StringResourcesCache;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by nushmodin on 23.05.2014.
 */
public class PlanSetDao extends AbstractDao<PlanSet, Long> {
    public PlanSetDao(Dao<PlanSet, Long> dao) throws SQLException {
        super(dao);
    }

    public PlanSet findNext(long prevSetId) {
        try {
            long nextTaskId = queryRawLong(StringResourcesCache.get("/db/v1/query/find_next_set.sql"), String.valueOf(prevSetId));
            return dao.queryForId(nextTaskId);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public PlanSet findFirstSetOfNextTask(long prevSetId) {
        try {
            long nextTaskId = queryRawLong(StringResourcesCache.get("/db/v1/query/find_first_set_of_next_task.sql"), String.valueOf(prevSetId));
            return dao.queryForId(nextTaskId);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<PlanSet> gePlanSetsForTask(long planTaskId) {
        try {
            return dao.query(dao.queryBuilder().where().eq("plan_task_id", planTaskId).prepare());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public PlanSet findFirstPlanSetByTask(long taskId) {
        try {
            long setId = queryRawLong(StringResourcesCache.get("/db/v1/query/find_first_set.sql"), String.valueOf(taskId));
            return dao.queryForId(setId);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public long findMaxVersion() {
        try {
            return queryRawLong("select max(version) from plan_set");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
