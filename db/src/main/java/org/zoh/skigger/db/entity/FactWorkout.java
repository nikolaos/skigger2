package org.zoh.skigger.db.entity;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by nushmodin on 23.05.2014.
 */
@DatabaseTable(tableName = "fact_workout")
public class FactWorkout {
    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField(foreign = true, columnName = "plan_workout_id", canBeNull = false)
    private PlanWorkout planWorkout;
    @DatabaseField(dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    private Date created;
    @DatabaseField
    private Boolean skipped;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PlanWorkout getPlanWorkout() {
        return planWorkout;
    }

    public void setPlanWorkout(PlanWorkout planWorkout) {
        this.planWorkout = planWorkout;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Boolean getSkipped() {
        return skipped;
    }

    public void setSkipped(Boolean skipped) {
        this.skipped = skipped;
    }
}
