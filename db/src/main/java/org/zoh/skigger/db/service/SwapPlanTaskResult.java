package org.zoh.skigger.db.service;

/**
 * Created by nushmodin on 03.06.2014.
 */
public class SwapPlanTaskResult {
    long newTaskAId;
    long newTaskBId;

    long nextWeekTaskAId;
    long nextWeekTaskBId;

    public SwapPlanTaskResult() {
    }

    public SwapPlanTaskResult(long newTaskAId, long newTaskBId, long nextWeekTaskAId, long nextWeekTaskBId) {
        this.newTaskAId = newTaskAId;
        this.newTaskBId = newTaskBId;
        this.nextWeekTaskAId = nextWeekTaskAId;
        this.nextWeekTaskBId = nextWeekTaskBId;
    }

    public long getNewTaskAId() {
        return newTaskAId;
    }

    public void setNewTaskAId(long newTaskAId) {
        this.newTaskAId = newTaskAId;
    }

    public long getNewTaskBId() {
        return newTaskBId;
    }

    public void setNewTaskBId(long newTaskBId) {
        this.newTaskBId = newTaskBId;
    }

    public long getNextWeekTaskAId() {
        return nextWeekTaskAId;
    }

    public void setNextWeekTaskAId(long nextWeekTaskAId) {
        this.nextWeekTaskAId = nextWeekTaskAId;
    }

    public long getNextWeekTaskBId() {
        return nextWeekTaskBId;
    }

    public void setNextWeekTaskBId(long nextWeekTaskBId) {
        this.nextWeekTaskBId = nextWeekTaskBId;
    }
}
