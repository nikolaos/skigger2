package org.zoh.skigger.db.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by nushmodin on 23.05.2014.
 */
@DatabaseTable
public class Program {
    @DatabaseField(generatedId = true, columnName = "id")
    private Long id;
    @DatabaseField(canBeNull = false, defaultValue = "0")
    private long version;
    @DatabaseField(width = 30)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
}
