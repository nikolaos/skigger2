package org.zoh.skigger.db.dao;

import com.j256.ormlite.dao.Dao;
import org.zoh.skigger.db.entity.FactTask;
import org.zoh.skigger.util.StringResourcesCache;

import java.sql.SQLException;

/**
 * Created by nushmodin on 23.05.2014.
 */
public class FactTaskDao extends AbstractDao<FactTask, Long> {
    public FactTaskDao(Dao<FactTask, Long> dao) throws SQLException {
        super(dao);
    }

    public int markTaskSkipped(long factTaskId, boolean skipped) {
        try {
            return dao.updateRaw("update fact_task set skipped = ? where id = ?", String.valueOf(skipped ? 1 : 0), String.valueOf(factTaskId));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public FactTask findLastTask() {
        try {
            long id = queryRawLong(StringResourcesCache.get("/db/v1/query/find_last_task.sql"));
            return dao.queryForId(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
