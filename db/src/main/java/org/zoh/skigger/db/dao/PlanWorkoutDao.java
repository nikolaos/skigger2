package org.zoh.skigger.db.dao;

import com.j256.ormlite.dao.Dao;
import org.zoh.skigger.db.entity.PlanWorkout;
import org.zoh.skigger.util.StringResourcesCache;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by nushmodin on 23.05.2014.
 */
public class PlanWorkoutDao extends AbstractDao<PlanWorkout, Long> {

    public PlanWorkoutDao(Dao<PlanWorkout, Long> dao) throws SQLException {
        super(dao);
    }

    public List<PlanWorkout> findByProgramId(long id) {
        try {
            return dao.queryForEq("program_id", id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public PlanWorkout findNext() {
        try {
            long id = queryRawLong(StringResourcesCache.get("/db/v1/query/find_next_workout.sql"));
            return dao.queryForId(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public long findMaxVersion() {
        try {
            return queryRawLong("select max(version) from plan_workout");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
