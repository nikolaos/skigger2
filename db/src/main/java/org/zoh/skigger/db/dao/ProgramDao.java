package org.zoh.skigger.db.dao;

import com.j256.ormlite.dao.Dao;
import org.zoh.skigger.db.entity.Program;

import java.sql.SQLException;

/**
 * Created by nushmodin on 23.05.2014.
 */
public class ProgramDao extends AbstractDao<Program, Long> {
    public ProgramDao(Dao<Program, Long> dao) throws SQLException {
        super(dao);
    }

    public long findMaxVersion() {
        try {
            return queryRawLong("select max(version) from program");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
