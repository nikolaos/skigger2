select npw.id
from fact_workout fw
 join plan_workout pw on pw.id = fw.plan_workout_id
 cross join plan_workout npw
where fw.created = (select max(w1.created) from fact_workout w1)
  and npw.no = (select min(npw1.no) from plan_workout npw1 where npw1.program_id = pw.program_id and npw1.no > pw.no)