select nps.id plan_set_id
  from plan_set nps
  join plan_set ps on ps.plan_task_id = nps.plan_task_id
 where ps.id = ?
    and coalesce(nps.started,datetime('now', 'localtime')) = (select coalesce(max(nps1.started), datetime('now', 'localtime'))
                                                   from plan_set nps1
                                                  where nps.plan_task_id = nps1.plan_task_id
                                                    and nps.no = nps1.no
                                                    and coalesce(nps1.started, datetime('now', 'localtime')) <= datetime('now', 'localtime'))
    and coalesce(nps.finished,datetime('now', 'localtime')) >= datetime('now', 'localtime')
    and nps.no = (select min(nps1.no)
                   from plan_set nps1
                  where nps1.plan_task_id = nps.plan_task_id
                    and nps1.no > ps.no)