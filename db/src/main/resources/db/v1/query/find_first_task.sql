select pt.id
  from plan_task pt
 where pt.id = (
  select max(pt.id)
    from plan_task pt
    where pt.plan_workout_id = ?
      and pt.no = (select min(pt1.no)
                     from plan_task pt1
                    where pt1.plan_workout_id = pt.plan_workout_id
                      and coalesce(pt1.started, '01-01-1970') <= datetime('now', 'localtime'))
      and coalesce(pt.started, '01-01-1970') = (select coalesce(max(pt1.started), '01-01-1970')
                                                 from plan_task pt1
                                                 where pt1.plan_workout_id = pt.plan_workout_id
                                                       and pt1.no = pt.no
                                                       and coalesce(pt1.started, '01-01-1970') <= datetime('now', 'localtime'))
      and coalesce(pt.finished,datetime('now', 'localtime')) >= datetime('now', 'localtime')
)