select fs.id
from fact_set fs
   join fact_task ft on ft.id = fs.fact_task_id
   join fact_workout fw on fw.id = ft.fact_workout_id
where fs.created = (select max(fs1.created) from fact_set fs1)
  and coalesce(ft.skipped,0) <> 1
  and coalesce(fw.skipped,0) <> 1
order by fs.id desc