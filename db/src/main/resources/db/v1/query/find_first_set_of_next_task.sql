select nps.id
  from plan_set nps
 where coalesce(nps.started,datetime('now', 'localtime')) = (select coalesce(max(nps1.started), datetime('now', 'localtime'))
                                                   from plan_set nps1
                                                  where nps.plan_task_id = nps1.plan_task_id
                                                    and nps.no = nps1.no
                                                    and coalesce(nps1.started, datetime('now', 'localtime')) <= datetime('now', 'localtime'))
    and coalesce(nps.finished,datetime('now', 'localtime')) >= datetime('now', 'localtime')
    and nps.no = (select min(nps1.no) from plan_set nps1 where nps1.plan_task_id = nps.plan_task_id)
    and nps.plan_task_id = (select min(npt1.id)
                              from plan_task npt1
                             cross join plan_set ps1 
                                   join plan_task pt1 on ps1.plan_task_id = pt1.id
                             where ps1.id = ?
                               and npt1.plan_workout_id = pt1.plan_workout_id
                               and npt1.no > pt1.no)
