select ps.id, ps.no, ps.amount, ps.count, ps.rest_time_sec
 from plan_set ps
where ps.plan_task_id = ?
  and ps.no > ?
  and coalesce(ps.start,datetime('now', 'localtime')) = (select coalesce(max(nps1.start), datetime('now', 'localtime'))
                                                 from plan_set nps1
                                                where ps.plan_task_id = nps1.plan_task_id
                                                  and ps.no = nps1.no
                                                  and nps1.start <= datetime('now', 'localtime'))
order by ps.no