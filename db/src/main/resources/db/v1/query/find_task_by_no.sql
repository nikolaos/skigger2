select pt.id, pt.name, pt.no, pt.start
  from plan_task pt
 where pt.plan_workout_id = ?
   and coalesce(pt.start, datetime('now', 'localtime')) = (select coalesce(max(pt1.start), datetime('now', 'localtime'))
                                                 from plan_task pt1
                                                where pt1.plan_workout_id = pt.plan_workout_id
                                                  and pt1.no = pt.no
                                                  and pt1.start <= datetime('now', 'localtime'))
   and pt.no = (select min(pt1.no)
                  from plan_task pt1
                 where pt1.plan_workout_id = pt.plan_workout_id
                   and pt1.no > ?)