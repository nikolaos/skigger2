select id
  from plan_task
 where id = (
  select max(npt.id)
    from plan_task npt
    join plan_task pt on pt.plan_workout_id = npt.plan_workout_id
   where pt.id = ?
      and coalesce(npt.started, '01-01-1970') = (select coalesce(max(pt1.started), '01-01-1970')
                                                   from plan_task pt1
                                                  where pt1.plan_workout_id = npt.plan_workout_id
                                                    and pt1.no = npt.no
                                                    and coalesce(pt1.started, '01-01-1970') <= datetime('now', 'localtime'))
      and coalesce(npt.finished,datetime('now', 'localtime')) >= datetime('now', 'localtime')
      and npt.no = (select min(pt1.no)
                      from plan_task pt1
                     where pt1.plan_workout_id = npt.plan_workout_id
                       and pt1.no > pt.no)
)