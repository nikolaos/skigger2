select ps.id
from plan_set ps
where ps.plan_task_id = ?
      and ps.no = (select min(ps1.no) from plan_set ps1 where ps1.plan_task_id = ps.plan_task_id)
      and coalesce(ps.finished,datetime('now', 'localtime')) >= datetime('now', 'localtime')
      and coalesce(ps.started, '01-01-1970') = (select coalesce(max(ps1.started), '01-01-1970')
                                                from plan_set ps1
                                                where ps1.plan_task_id = ps.plan_task_id
                                                      and ps1.no = ps.no
                                                      and coalesce(ps1.started, '01-01-1970') <= datetime('now', 'localtime'))