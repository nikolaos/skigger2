package ru.nikolaos.skigger.db;

import android.content.res.Resources;
import android.util.Log;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.zoh.skigger.db.service.SkiggerService;

/**
 * Created by nushmodin on 14.07.2014.
 */
@Singleton
public class DbService extends SkiggerService {
    private static final String LOG_TAG = DbService.class.getSimpleName();

    @Inject
    public DbService(Database daoFactory) {
        super(daoFactory.getDaoFactory());
        try {
            daoFactory.execute(org.zoh.skigger.util.Resources.readResourceToStrings("/db/init_data.sql"));
        } catch (Exception e) {
            Log.e(LOG_TAG, "Can't load init script", e);
        }
    }
}
