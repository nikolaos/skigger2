package ru.nikolaos.skigger.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import org.zoh.skigger.db.DaoFactory;
import org.zoh.skigger.db.service.SkiggerService;
import org.zoh.skigger.util.Resources;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by nushmodin on 23.05.2014.
 */
@Singleton
public class Database extends OrmLiteSqliteOpenHelper {
    private static final String TAG = Database.class.getName();
    private static final String DATABASE_NAME = "skigger.db";
    private static final int DATABASE_VERSION = 1;

    private final File databasePath;
    private final DaoFactory daoFactory;

    @Inject
    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        databasePath = context.getDatabasePath(DATABASE_NAME);
        daoFactory = new DaoFactory(getConnectionSource());
    }

    DaoFactory getDaoFactory() {
        return daoFactory;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try
        {
            DaoFactory.createTables(connectionSource);
        }
        catch (Exception e){
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try
        {
            DaoFactory.dropTables(connectionSource);
            onCreate(database, connectionSource);
        }
        catch (SQLException e){
            Log.e(TAG, "error upgrading DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }


    public void drop() {
        Log.d(TAG, "Drop database " + DATABASE_NAME);
        databasePath.delete();
    }

    public void execute(List<String> queries) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            for (String sql : queries) {
                Log.d(TAG, "Exec: " + sql);
                db.execSQL(sql);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }

    }
}
