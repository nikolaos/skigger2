package ru.nikolaos.skigger.task;

/**
 * Created by nushmodin on 08.09.2014.
 */
public interface TaskListener<T> {
    void onStart();
    void onSuccess(T res);
    void onError(Exception e);
}
