package ru.nikolaos.skigger.task;

import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.*;

/**
 * Created by nushmodin on 08.09.2014.
 */
public class RestClient {
    public static final String APPLICATION_JSON = "application/json";
    private final HttpClient httpClient = new DefaultHttpClient();
    private final HttpHost host;

    public RestClient(String host) {
        this.host = new HttpHost(host);
    }

    protected byte [] post(String uri, byte []body) throws IOException, HttpException {
        HttpPost post = new HttpPost(uri);
        ByteArrayEntity entity = new ByteArrayEntity(body);
        entity.setContentType(APPLICATION_JSON);
        post.setEntity(entity);

        HttpEntity httpEntity = null;
        try {
            HttpResponse httpResponse = httpClient.execute(host, post);

            StatusLine statusLine = httpResponse.getStatusLine();
            if (statusLine.getStatusCode() != 200) {
                throw new HttpException(statusLine.getReasonPhrase());
            }

            httpEntity = httpResponse.getEntity();
            if (!APPLICATION_JSON.equals(httpEntity.getContentType())) {
                throw new HttpException("Response content type must be " + APPLICATION_JSON);
            }

            return toBytes(httpEntity.getContent());
        } finally {
            if (httpEntity != null) {
                httpEntity.consumeContent();
            }
        }
    }

    protected String get(String uri) throws IOException, HttpException {
        HttpEntity httpEntity = null;
        try {
            HttpResponse httpResponse = httpClient.execute(host, new HttpGet(uri));

            StatusLine statusLine = httpResponse.getStatusLine();
            if (statusLine.getStatusCode() != 200) {
                throw new HttpException(statusLine.getReasonPhrase());
            }

            httpEntity = httpResponse.getEntity();
            if (!APPLICATION_JSON.equals(httpEntity.getContentType())) {
                throw new HttpException("Response content type must be " + APPLICATION_JSON);
            }

            return toString(httpEntity.getContent());
        } finally {
            if (httpEntity != null) {
                httpEntity.consumeContent();
            }
        }
    }

    private static byte[] toBytes(InputStream content) throws IOException {
        byte [] buffer = new byte[1024];
        int len;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            while ((len = content.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len);
            }
            return outputStream.toByteArray();
        } finally {
            outputStream.close();
        }
    }

    private static String toString(InputStream content) throws IOException {
        char [] buffer = new char[1024];
        int len;

        Writer writer = new StringWriter();
        Reader reader = new InputStreamReader(content, "UTF-8");
        try {
            while ((len = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, len);
            }
            return writer.toString();
        } finally {
            writer.close();
        }
    }

}
