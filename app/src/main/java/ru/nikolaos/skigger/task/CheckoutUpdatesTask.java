package ru.nikolaos.skigger.task;

import android.os.AsyncTask;
import android.util.JsonReader;
import org.apache.http.HttpException;
import org.zoh.skigger.db.entity.PlanSet;
import org.zoh.skigger.db.entity.PlanTask;
import org.zoh.skigger.db.entity.PlanWorkout;
import org.zoh.skigger.db.entity.Program;
import org.zoh.skigger.db.service.SkiggerService;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nushmodin on 08.09.2014.
 */
public class CheckoutUpdatesTask extends AsyncTask<Void, Void, CheckoutUpdatesTask.Result> {
    private static final String URI = "/rest/dictionary/";
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final RestClient restClient;
    private final SkiggerService skiggerService;
    private final TaskListener<Result> listener;

    public static class Result {
        private List<Program> programs = new ArrayList<Program>();
        private List<PlanSet> planSets = new ArrayList<PlanSet>();
        private List<PlanTask> planTasks = new ArrayList<PlanTask>();
        private List<PlanWorkout> planWorkouts = new ArrayList<PlanWorkout>();
        private Exception exception;

        public List<Program> getPrograms() {
            return programs;
        }

        public List<PlanSet> getPlanSets() {
            return planSets;
        }

        public List<PlanTask> getPlanTasks() {
            return planTasks;
        }

        public List<PlanWorkout> getPlanWorkouts() {
            return planWorkouts;
        }

        public Exception getException() {
            return exception;
        }
    }

    public CheckoutUpdatesTask(String host, SkiggerService skiggerService, TaskListener<Result> listener) {
        this.restClient = new RestClient(host);
        this.skiggerService = skiggerService;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        listener.onStart();
    }

    @Override
    protected Result doInBackground(Void... voids) {
        String uri = URI + skiggerService.getDictionariesVersion();

        Result result = new Result();
        try {
            read(new JsonReader(new StringReader(restClient.get(uri))), result);
        } catch (Exception e) {
            result.exception = e;
            listener.onError(e);
        }

        return result;
    }

    @Override
    protected void onPostExecute(Result result) {
        skiggerService.update(result.programs, result.planWorkouts, result.planTasks, result.planSets);
        listener.onSuccess(result);
        super.onPostExecute(result);
    }

    private void read(JsonReader reader, Result result) throws IOException {
        reader.beginObject();

        String name = reader.nextName();
        if ("program".equals(name)) {
            result.programs.addAll(readPrograms(reader));
        } else if ("plan_workout".equals(name)) {
            result.planWorkouts.addAll(readPlanWorkout(reader));
        } else if ("plan_task".equals(name)) {
            result.planTasks.addAll(readPlanTask(reader));
        } else if ("plan_set".equals(name)) {
            result.planSets.addAll(readPlanSet(reader));
        }

        reader.endObject();
    }

    private List<PlanWorkout> readPlanWorkout(JsonReader reader) throws IOException {
        reader.beginArray();

        List<PlanWorkout> list = new ArrayList<PlanWorkout>();
        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                PlanWorkout workout = new PlanWorkout();
                if ("id".equals(name)) {
                    workout.setId(reader.nextLong());
                } else if ("name".equals(name)) {
                    workout.setName(reader.nextString());
                } else if ("no".equals(name)) {
                    workout.setNo(reader.nextInt());
                } else if ("version".equals(name)) {
                    workout.setVersion(reader.nextLong());
                } else if ("program_id".equals(name)) {
                    Program program = new Program();
                    program.setId(reader.nextLong());
                    workout.setProgram(program);
                }
                list.add(workout);
            }
            reader.endObject();
        }

        reader.endArray();

        return list;
    }

    private List<PlanSet> readPlanSet(JsonReader reader) throws IOException {
        reader.beginArray();

        List<PlanSet> list = new ArrayList<PlanSet>();
        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                PlanSet planSet = new PlanSet();
                if ("id".equals(name)) {
                    planSet.setId(reader.nextLong());
                } else if ("no".equals(name)) {
                    planSet.setNo(reader.nextInt());
                } else if ("amount".equals(name)) {
                    planSet.setAmount((float) reader.nextDouble());
                } else if ("count".equals(name)) {
                    planSet.setCount(reader.nextInt());
                } else if ("rest_time_sec".equals(name)) {
                    planSet.setRestTimeSec(reader.nextInt());
                } else if ("started".equals(name)) {
                    try {
                        planSet.setStarted(DATE_FORMAT.parse(reader.nextString()));
                    } catch (ParseException e) {
                        throw new IOException("Can't parse date for field started", e);
                    }
                } else if ("finished".equals(name)) {
                    try {
                        planSet.setFinished(DATE_FORMAT.parse(reader.nextString()));
                    } catch (ParseException e) {
                        throw new IOException("Can't parse date for field finished", e);
                    }
                } else if ("plan_task_id".equals(name)) {
                    PlanTask task = new PlanTask();
                    task.setId(reader.nextLong());
                    planSet.setPlanTask(task);
                }
                list.add(planSet);
            }
            reader.endObject();
        }

        reader.endArray();
        return list;
    }

    private List<PlanTask> readPlanTask(JsonReader reader) throws IOException {
        reader.beginArray();

        List<PlanTask> list = new ArrayList<PlanTask>();
        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                PlanTask task = new PlanTask();
                if ("id".equals(name)) {
                    task.setId(reader.nextLong());
                } else if ("name".equals(name)) {
                    task.setName(reader.nextString());
                } else if ("no".equals(name)) {
                    task.setNo(reader.nextInt());
                } else if ("version".equals(name)) {
                    task.setVersion(reader.nextLong());
                } else if ("started".equals(name)) {
                    try {
                        task.setStarted(DATE_FORMAT.parse(reader.nextString()));
                    } catch (ParseException e) {
                        throw new IOException("Can't parse date for field started", e);
                    }
                } else if ("finished".equals(name)) {
                    try {
                        task.setFinished(DATE_FORMAT.parse(reader.nextString()));
                    } catch (ParseException e) {
                        throw new IOException("Can't parse date for field finished", e);
                    }
                } else if ("plan_workout_id".equals(name)) {
                    PlanWorkout workout = new PlanWorkout();
                    workout.setId(reader.nextLong());
                    task.setPlanWorkout(workout);
                }
                list.add(task);
            }
            reader.endObject();
        }

        reader.endArray();
        return list;
    }


    private List<Program> readPrograms(JsonReader reader) throws IOException {
        reader.beginArray();

        List<Program> list = new ArrayList<Program>();
        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                Program program = new Program();
                if ("id".equals(name)) {
                    program.setId(reader.nextLong());
                } else if ("version".equals(name)) {
                    program.setVersion(reader.nextLong());
                } else if ("name".equals(name)) {
                    program.setName(reader.nextString());
                }

                list.add(program);
            }
            reader.endObject();
        }

        reader.endArray();

        return list;
    }


}
