package ru.nikolaos.skigger.util;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by nikolay on 17.04.14.
 */
public abstract class CustomArrayAdapter<T> extends ArrayAdapter<T> {
    private Activity context;

    public CustomArrayAdapter(Activity context, T [] list) {
        super(context, android.R.layout.simple_spinner_item, list);
        this.context = context;
    }

    public CustomArrayAdapter(Activity context) {
        super(context, android.R.layout.simple_spinner_item);
        this.context = context;
    }



    protected abstract CharSequence getTextForView(T item, int position);

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        TextView textView = (TextView)inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        textView.setText(getTextForView(getItem(position), position));
        return textView;
    }
}
