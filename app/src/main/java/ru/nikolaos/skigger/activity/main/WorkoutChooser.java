package ru.nikolaos.skigger.activity.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import org.zoh.skigger.db.entity.PlanWorkout;
import org.zoh.skigger.db.entity.Program;
import org.zoh.skigger.db.service.SkiggerService;
import ru.nikolaos.skigger.R;
import ru.nikolaos.skigger.activity.workout.WorkoutActivity;
import ru.nikolaos.skigger.util.CustomArrayAdapter;

import java.util.NoSuchElementException;

/**
 * Created by nushmodin on 16.04.2014.
 */
class WorkoutChooser implements DialogInterface.OnClickListener, AdapterView.OnItemSelectedListener, DialogInterface.OnShowListener {
    private final Activity parent;

    private final Spinner programsSpnr;
    private final ArrayAdapter<Program> programsSpnrAdapter;
    private final Spinner workoutSpnr;
    private final ArrayAdapter<PlanWorkout> workoutSpnrAdapter;

    private final Dialog dialog;
    private final SkiggerService skiggerService;

    WorkoutChooser(Activity parent, SkiggerService skiggerService) {
        this.parent = parent;
        this.skiggerService = skiggerService;

        View view = parent.getLayoutInflater().inflate(R.layout.dlg_workout_choose, null);

        workoutSpnrAdapter = new CustomArrayAdapter<PlanWorkout>(parent) {
            @Override
            protected CharSequence getTextForView(PlanWorkout item, int position) {
                return item.getName();
            }
        };

        workoutSpnr = (Spinner)view.findViewById(R.id.workoutChsrWorkoutSpnr);
        workoutSpnr.setAdapter(workoutSpnrAdapter);
        workoutSpnr.setOnItemSelectedListener(this);

        programsSpnrAdapter = new CustomArrayAdapter<Program>(parent) {
            @Override
            protected CharSequence getTextForView(Program item, int position) {
                return item.getName();
            }
        };

        programsSpnr = (Spinner)view.findViewById(R.id.workoutChsrProgramSpnr);
        programsSpnr.setAdapter(programsSpnrAdapter);
        for (Program program : skiggerService.getPrograms()) {
            programsSpnrAdapter.add(program);
        }
        programsSpnr.setOnItemSelectedListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(parent);
        builder.setView(view);
        builder.setPositiveButton(R.string.workout_choouse_ok, this);
        builder.setNegativeButton(R.string.workout_choouse_cancel, this);
        builder.setCancelable(true);

        dialog = builder.create();
        dialog.setOnShowListener(this);
    }

    public void show() {
        dialog.show();
    }


    private void onPositiveClick(DialogInterface dialogInterface) {
        dialogInterface.dismiss();
        PlanWorkout selectedWorkout = (PlanWorkout) workoutSpnr.getSelectedItem();
        WorkoutActivity.showByPlanWorkoutId(parent, selectedWorkout.getId());
    }

    private void onProgramSelected(Program program) {
        workoutSpnrAdapter.clear();
        for (PlanWorkout workout : skiggerService.getPlanWorkouts(program.getId())) {
            workoutSpnrAdapter.add(workout);
        }
    }

    private void onNegativeClick(DialogInterface dialogInterface) {
        dialogInterface.dismiss();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            onPositiveClick(dialogInterface);
        } else if (which == DialogInterface.BUTTON_NEGATIVE) {
            onNegativeClick(dialogInterface);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        if (adapterView == programsSpnr) {
            onProgramSelected(programsSpnrAdapter.getItem(position));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @Override
    public void onShow(DialogInterface dialogInterface) {
        try {
            PlanWorkout nextWorkout = skiggerService.findNextWorkout();
            for (int i = 0, size = workoutSpnrAdapter.getCount(); i < size; i++) {
                if (workoutSpnrAdapter.getItem(i).getId() == nextWorkout.getId()) {
                    workoutSpnr.setSelection(i, true);
                }
            }
        } catch (NoSuchElementException e) {
            workoutSpnr.setSelection(0);
        }
    }
}
