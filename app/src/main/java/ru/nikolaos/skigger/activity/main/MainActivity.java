package ru.nikolaos.skigger.activity.main;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import com.google.inject.Inject;
import org.zoh.skigger.db.entity.FactSet;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import ru.nikolaos.skigger.R;
import ru.nikolaos.skigger.activity.workout.WorkoutActivity;
import ru.nikolaos.skigger.db.DbService;
import ru.nikolaos.skigger.task.CheckoutUpdatesTask;
import ru.nikolaos.skigger.task.TaskListener;

import java.util.NoSuchElementException;


public class MainActivity extends RoboActivity implements OnClickListener {
    private WorkoutChooser chooseWorkoutDialog;
    @InjectView(R.id.main_resumeBtn)
    private Button resumeBtn;
    @InjectView(R.id.main_checkoutBrn)
    private Button checkoutBtn;
    @Inject
    private DbService dbService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        chooseWorkoutDialog = new WorkoutChooser(this, dbService);

        findViewById(R.id.main_startGymBtn).setOnClickListener(this);
        resumeBtn.setOnClickListener(this);
        checkoutBtn.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            FactSet lastFactSet = dbService.findLastFactSet();
            dbService.findNextPlanSet(lastFactSet.getPlanSet().getId());
            resumeBtn.setEnabled(true);
        } catch (NoSuchElementException e) {
            resumeBtn.setEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
    	switch (v.getId()) {
		    case R.id.main_startGymBtn:
                chooseWorkoutDialog.show();
			    break;
            case R.id.main_resumeBtn:
                resumeUnderdoneGym();
                break;
            case R.id.main_checkoutBrn:
                new CheckoutUpdatesTask("", dbService, new TaskListener<CheckoutUpdatesTask.Result>() {
                    @Override
                    public void onStart() {
                        Toast.makeText(MainActivity.this, "Started", Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onSuccess(CheckoutUpdatesTask.Result res) {
                        Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onError(Exception e) {
                        Toast.makeText(MainActivity.this, "Error " + e.getMessage(), Toast.LENGTH_SHORT);
                    }
                }).execute();
                break;
		    default:
			    break;
		}
    }

    private void resumeUnderdoneGym() {
        WorkoutActivity.showByLastSet(this);
    }

}
