package ru.nikolaos.skigger.activity.workout;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.google.inject.Inject;
import org.apache.http.client.methods.HttpPost;
import org.zoh.skigger.db.entity.FactSet;
import org.zoh.skigger.db.entity.PlanSet;
import org.zoh.skigger.model.WorkoutModel;
import org.zoh.skigger.model.WorkoutState;
import org.zoh.skigger.util.Strings;
import org.zoh.skigger.util.TimeFormatter;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import ru.nikolaos.skigger.R;
import ru.nikolaos.skigger.db.DbService;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class WorkoutActivity extends RoboActivity implements View.OnClickListener {
    private static enum ActivityMode {
        BY_PLAN_WORKOUT_ID, BY_LAST_SET
    }

    private static final String ACTIVITY_MODE = "activity_mode";
    private static final String PLAN_WORKOUT_ID = "plan.workout_id";
    private static final NumberFormat AMOUNT_FORMAT = new DecimalFormat("0.##");

    @InjectView(R.id.workoutDayTxt)
    private TextView dayNoTxt;
    @InjectView(R.id.workoutTimeTxt)
    private TextView workoutTimeTxt;
    @InjectView(R.id.workoutTaskTxt)
    private TextView taskNameTxt;
    @InjectView(R.id.workoutSetNoTxt)
    private TextView setNoTxt;
    @InjectView(R.id.workoutTaskTimeTxt)
    private TextView taskTimeTxt;
    @InjectView(R.id.workoutRestTxt)
    private TextView restTxt;
    @InjectView(R.id.workoutSetAmountEdt)
    private EditText setAmountEdt;
    @InjectView(R.id.workoutSetCountEdt)
    private EditText setCountEdt;
    @InjectView(R.id.workoutNextSetAmountEdt)
    private EditText nextSetAmountEdt;
    @InjectView(R.id.workoutNextSetCountEdt)
    private EditText nextSetCountEdt;
    @InjectView(R.id.workoutMainBtn)
    private Button mainBtn;
    @InjectView(R.id.workoutSkipBtn)
    private Button skipBtn;
    @InjectResource(R.string.workout_mainbtn_rest)
    private String mainBtnRestText;
    @InjectResource(R.string.workout_mainbtn_go)
    private String mainBtnGoText;
    @InjectResource(R.string.workout_mainbtn_start)
    private String mainBtnStartText;
    @Inject
    private DbService dbService;

    private Timer rendererTimer = new Timer();
    @Inject
    private TimeFormatter timeFormatter;
    private WorkoutModel workoutData;


    private AmountEditor amountEditor;


    public static void showByLastSet(Activity parent) {
        Intent intent = new Intent(parent, WorkoutActivity.class);
        intent.putExtra(WorkoutActivity.ACTIVITY_MODE, WorkoutActivity.ActivityMode.BY_LAST_SET);
        parent.startActivity(intent);
    }

    public static void showByPlanWorkoutId(Activity parent, long planWorkoutId) {
        Intent intent = new Intent(parent, WorkoutActivity.class);
        intent.putExtra(WorkoutActivity.ACTIVITY_MODE, WorkoutActivity.ActivityMode.BY_PLAN_WORKOUT_ID);
        intent.putExtra(WorkoutActivity.PLAN_WORKOUT_ID, planWorkoutId);
        parent.startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        rendererTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (workoutData.getWorkout().getState() != WorkoutState.STARTED) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateTimeViews();
                        }
                    });
                }
            }
        }, 0, 500);
    }

    @Override
    protected void onStop() {
        super.onStop();
        rendererTimer.cancel();
        rendererTimer.purge();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        initView();

        Bundle extras = getIntent().getExtras();
        final ActivityMode mode = (ActivityMode) extras.get(ACTIVITY_MODE);
        switch (mode) {
            case BY_PLAN_WORKOUT_ID:
                long workoutId = extras.getLong(PLAN_WORKOUT_ID);
                workoutData = WorkoutModel.createByWorkout(workoutId, dbService);
                break;
            case BY_LAST_SET:
                FactSet lastFactSet = dbService.findLastFactSet();
                PlanSet nextPlanSet = dbService.findNextPlanSet(lastFactSet.getPlanSet().getId());
                workoutData = WorkoutModel.createByPlanSet(nextPlanSet.getId(), dbService);
                break;
            default:
        }

        updateTimeViews();

        amountEditor = new AmountEditor(this);
    }

    public void updateTimeViews() {
        Date workoutCreated = workoutData.getWorkout().getCreated();
        if (workoutCreated != null) {
            workoutTimeTxt.setText(timeFormatter.formatPeriod(workoutCreated, new Date()));
        }
        dayNoTxt.setText(String.valueOf(workoutData.getWorkout().getNo()));
        taskNameTxt.setText(workoutData.getTask().getName());
        if (workoutData.getTask().getCreated() != null) {
            taskTimeTxt.setText(timeFormatter.formatPeriod(workoutData.getTask().getCreated(), new Date()));
        }
        setNoTxt.setText(String.valueOf(workoutData.getSet().getNo()));
        if (workoutData.getWorkout().getState() == WorkoutState.REST && workoutData.getSet().getPlanRestEndTime() != null) {
            restTxt.setText(timeFormatter.formatPeriod(new Date(), workoutData.getSet().getPlanRestEndTime()));
        }
        setAmountEdt.setText(AMOUNT_FORMAT.format(workoutData.getSet().getFactAmount()));
        setCountEdt.setText(String.valueOf(workoutData.getSet().getFactCount()));

        nextSetAmountEdt.setText(Strings.nullToEmpty(workoutData.getSet().getNextPlanAmount()));
        nextSetCountEdt.setText(Strings.nullToEmpty(workoutData.getSet().getNextPlanCount()));
        switch (workoutData.getWorkout().getState()) {
            case STARTED:
                mainBtn.setText(mainBtnStartText);
                restTxt.setText(null);
                break;
            case WORK:
                mainBtn.setText(mainBtnRestText);
                restTxt.setText(null);
                break;
            case REST:
                mainBtn.setText(mainBtnGoText);
                break;
        }
    }

    private void initView() {
        setAmountEdt.setOnClickListener(this);
        setCountEdt.setOnClickListener(this);
        restTxt.setText(null);
        mainBtn.setOnClickListener(this);
        skipBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.workoutMainBtn:
                boolean hasNext = workoutData.shift();
                if (!hasNext) {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.workout_complete_title)
                            .setMessage(R.string.workout_complete_message)
                            .setPositiveButton(R.string.workout_complete_ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                }
                            })
                            .show();
                }
                break;
            case R.id.workoutSetAmountEdt:
            case R.id.workoutSetCountEdt:
                amountEditor.editFact(workoutData.getSet());
                break;
            case R.id.workoutSkipBtn:
                workoutData.skipSet();
                updateTimeViews();
                break;
        }
    }

}
