package ru.nikolaos.skigger.activity.workout;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import org.zoh.skigger.model.Set;
import ru.nikolaos.skigger.R;

/**
 * Created by nushmodin on 06.05.2014.
 */
class AmountEditor implements View.OnClickListener, DialogInterface.OnClickListener {
    private final WorkoutActivity activity;
    private final Dialog dialog;
    private final EditText amountEdt;
    private final EditText countEdt;
    private Set currentSet;
    private float amount;
    private int count;

    public AmountEditor(WorkoutActivity activity) {
        this.activity = activity;

        View view = activity.getLayoutInflater().inflate(R.layout.dlg_amount_editor, null);

        view.findViewById(R.id.dlgAmountEditor_amountUp5Btn).setOnClickListener(this);
        view.findViewById(R.id.dlgAmountEditor_amountUp2_5Btn).setOnClickListener(this);
        view.findViewById(R.id.dlgAmountEditor_amountUp1_25Btn).setOnClickListener(this);
        view.findViewById(R.id.dlgAmountEditor_amountDown5Btn).setOnClickListener(this);
        view.findViewById(R.id.dlgAmountEditor_amountDown2_5Btn).setOnClickListener(this);
        view.findViewById(R.id.dlgAmountEditor_amountDown1_25Btn).setOnClickListener(this);

        view.findViewById(R.id.dlgAmountEditor_countUp1Btn).setOnClickListener(this);
        view.findViewById(R.id.dlgAmountEditor_countDown1Btn).setOnClickListener(this);

        amountEdt = (EditText)view.findViewById(R.id.dlgAmountEditor_amountEdt);
        countEdt = (EditText)view.findViewById(R.id.dlgAmountEditor_countEdt);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(view);
        builder.setPositiveButton(R.string.workout_choouse_ok, this);
        builder.setNegativeButton(R.string.workout_choouse_cancel, this);
        builder.setCancelable(true);

        dialog = builder.create();
    }


    public void editFact(Set set) {
        this.currentSet = set;

        this.count = set.getFactCount();
        this.amount = set.getFactAmount();

        amountEdt.setText(String.valueOf(amount));
        countEdt.setText(String.valueOf(count));

        dialog.show();
    }


    public void rollAmount(float value) {
        amount += value;
        amountEdt.setText(String.valueOf(amount));
    }

    public void rollCount(int value) {
        count += value;
        countEdt.setText(String.valueOf(count));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dlgAmountEditor_amountUp5Btn:
                rollAmount(5);
                break;
            case R.id.dlgAmountEditor_amountUp2_5Btn:
                rollAmount(2.5f);
                break;
            case R.id.dlgAmountEditor_amountUp1_25Btn:
                rollAmount(1.25f);
                break;
            case R.id.dlgAmountEditor_amountDown1_25Btn:
                rollAmount(-1.25f);
                break;
            case R.id.dlgAmountEditor_amountDown2_5Btn:
                rollAmount(-2.5f);
                break;
            case R.id.dlgAmountEditor_amountDown5Btn:
                rollAmount(-5);
                break;
            case R.id.dlgAmountEditor_countUp1Btn:
                rollCount(1);
                break;
            case R.id.dlgAmountEditor_countDown1Btn:
                rollCount(-1);
                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (DialogInterface.BUTTON_POSITIVE == i) {
            currentSet.setFactAmount(amount);
            currentSet.setFactCount(count);
            activity.updateTimeViews();
        }
        dialogInterface.dismiss();
    }
}
