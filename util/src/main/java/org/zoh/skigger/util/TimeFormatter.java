package org.zoh.skigger.util;

import java.util.Date;

/**
 * Created by nikolay on 13.04.14.
 */
public class TimeFormatter {
    private String hourText = "h";
    private String minuteText = "m";
    private String secondText = "s";

    public TimeFormatter() {
    }

    public TimeFormatter(String hourText, String minuteText, String secondText) {
        this.hourText = hourText;
        this.minuteText = minuteText;
        this.secondText = secondText;
    }

    public String formatPeriod(Date from, Date to) {
        long times = (to.getTime() - from.getTime()) / 1000;
        boolean negative = times < 0;
        if (negative) {
            times = -times;
        }

        int sec = (int)(times % 60);
        int minutes = (int)((times % 3600) / 60);
        int hours = (int)(times / 3600);

        StringBuilder result = new StringBuilder(negative ? "-" : "");

        if (hours > 0) {
            result.append(hours).append(hourText).append(' ');
        }
        if (minutes > 0 || hours > 0) {
            result.append(minutes).append(minuteText).append(' ');
        }
        result.append(sec).append(secondText);

        return result.toString();
    }

    String getHourText() {
        return hourText;
    }

    String getMinuteText() {
        return minuteText;
    }

    String getSecondText() {
        return secondText;
    }
}
