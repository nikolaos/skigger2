package org.zoh.skigger.util;

/**
 * Created by nushmodin on 06.08.2014.
 */
public class Nums {
    public static <T extends Comparable> T max(T ...args) {
        if (args != null && args.length > 0) {
            T max = args[0];
            for (int i = 1; i < args.length; i++) {
                if (max.compareTo(args[i]) < 0) {
                    max = args[i];
                }
            }
            return max;
        }
        return null;
    }
}
