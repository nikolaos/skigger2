package org.zoh.skigger.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nushmodin on 14.05.2014.
 */
public class StringResourcesCache {
    private static Map<String, String> cache = new HashMap<String, String>();

    public static String get(String resource) {
        String content = cache.get(resource);
        if (content == null) {
            try {
                content = Resources.readResourceToString(resource);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
            cache.put(resource, content);
        }
        return content;
    }
}
