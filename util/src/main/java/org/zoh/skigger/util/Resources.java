package org.zoh.skigger.util;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ushmodin@gmail.com on 11.02.14.
 */
public class Resources {

    private static final int EOF = -1;
	private static final String UTF_8 = "UTF-8";

	/**
     * Read resource file in UTF-8 and return content as string
     * @param path path to resource
     * @return string content
     * @throws java.io.IOException
     */
    public static String readResourceToString(String path) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStream in = Resources.class.getResourceAsStream(path);
        if (in == null) {
        	throw new FileNotFoundException("Resource " + path + " not found");
        }
        try {
            int len;
            byte buffer[] = new byte[1024];
            while ((len = in.read(buffer)) != EOF) {
                sb.append(new String(buffer, 0, len, UTF_8));
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
        return sb.toString();
    }

    public static List<String> readResourceToStrings(String path) throws IOException {
        List<String> list = new ArrayList<String>();
        InputStream in = Resources.class.getResourceAsStream(path);
        if (in == null) {
        	throw new FileNotFoundException("Resource " + path + " not found");
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, UTF_8));
            String line;
            while ((line = reader.readLine()) != null) {
            	list.add(line);
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
        return list;
    }

    public static void close(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (Exception e) {
        }
    }
}
