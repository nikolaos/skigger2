package org.zoh.skigger.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by nushmodin on 02.06.2014.
 */
public class Dates {
    public static Date addWeek(Date now) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.WEEK_OF_YEAR, 1);
        return calendar.getTime();
    }

    public static Date now() {
        return new Date();
    }

    public static Date truncate(Date date, int field) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(field, 0);
        return calendar.getTime();
    }
}
