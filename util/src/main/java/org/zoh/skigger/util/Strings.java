package org.zoh.skigger.util;

/**
 * Created by nushmodin on 16.07.2014.
 */
public class Strings {

    public static final String EMPTY = "";

    public static String nullToEmpty(String o) {
        return o != null ? o : EMPTY;
    }
    public static String nullToEmpty(Object o) {
        return o != null ? o.toString() : EMPTY;
    }
}
