package org.zoh.skigger.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * Created by nushmodin on 22.04.2014.
 */
public class TimeFormatterTest {
    private TimeFormatter timeFormatter = new TimeFormatter();


    @Test
    public void testFormatPeriodSecond() throws Exception {
        Date now = new Date();
        Date from = new Date(now.getTime() - 10000L);
        Date to = new Date(now.getTime() + 10000L);

        String strDelta = timeFormatter.formatPeriod(from, to);

        Assert.assertEquals("Invalid delta", "20s", strDelta);
    }

    @Test
    public void testFormatPeriodMinutes() throws Exception {
        Date now = new Date();
        Date from = new Date(now.getTime());
        Date to = new Date(now.getTime() + 60000L);

        String strDelta = timeFormatter.formatPeriod(from, to);

        Assert.assertEquals("Invalid delta", "1m 0s", strDelta);
    }

    @Test
    public void testFormatPeriodMinSec() throws Exception {
        Date now = new Date();
        Date from = new Date(now.getTime());
        Date to = new Date(now.getTime() + 27 * 60 * 1000 + 48 * 1000);

        String strDelta = timeFormatter.formatPeriod(from, to);

        Assert.assertEquals("Invalid delta", "27m 48s", strDelta);
    }

    @Test
    public void testFormatPeriodHourMinSec() throws Exception {
        Date now = new Date();
        Date from = new Date(now.getTime());
        Date to = new Date(now.getTime() + 3 * 60 * 60 * 1000 + 2 * 60 * 1000 + 59 * 1000);

        String strDelta = timeFormatter.formatPeriod(from, to);

        Assert.assertEquals("Invalid delta", "3h 2m 59s", strDelta);
    }

    @Test
    public void testFormatPeriodHourSec() throws Exception {
        Date now = new Date();
        Date from = new Date(now.getTime() + 20 * 60 * 60 * 1000 + 7 * 1000);
        Date to = new Date(now.getTime());

        String strDelta = timeFormatter.formatPeriod(from, to);

        Assert.assertEquals("Invalid delta", "-20h 0m 7s", strDelta);
    }
}
